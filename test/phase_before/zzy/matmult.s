	.file	1 "matmult.c"
	.rdata
	.align	2
$LC0:
	.ascii	"C[%d][%d] = %d\n\000"
	.text
	.align	2
	.globl	main
	.ent	main
main:
	.frame	$sp,24,$31		# vars= 0, regs= 2/0, args= 16, extra= 0
	.mask	0x80010000,-4
	.fmask	0x00000000,0
	subu	$sp,$sp,24
	sw	$31,20($sp)
	.set	noreorder
	.set	nomacro
	jal	__main
	sw	$16,16($sp)
	.set	macro
	.set	reorder

	move	$11,$0
	la	$9,A
	la	$7,B
	la	$6,C
	sll	$2,$11,2
$L38:
	addu	$2,$2,$11
	sll	$5,$2,4
	move	$8,$0
$L10:
	addu	$2,$5,$7
	sw	$8,0($2)
	addu	$8,$8,1
	addu	$2,$5,$9
	addu	$3,$5,$6
	slt	$4,$8,20
	sw	$11,0($2)
	sw	$0,0($3)
	.set	noreorder
	.set	nomacro
	bne	$4,$0,$L10
	addu	$5,$5,4
	.set	macro
	.set	reorder

	addu	$11,$11,1
	slt	$2,$11,20
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L38
	sll	$2,$11,2
	.set	macro
	.set	reorder

	move	$11,$0
	la	$15,C
	la	$13,B
	la	$14,A
	sll	$2,$11,2
$L39:
	addu	$2,$2,$11
	sll	$2,$2,4
	move	$12,$2
	move	$8,$0
	addu	$9,$2,$15
$L25:
	sll	$2,$8,2
	lw	$7,0($9)
	addu	$5,$2,$13
	move	$10,$9
	addu	$4,$12,$14
	li	$6,19			# 0x13
$L24:
	lw	$2,0($4)
	lw	$3,0($5)
	addu	$6,$6,-1
	mult	$2,$3
	addu	$5,$5,80
	addu	$4,$4,4
	mflo	$2
	#nop
	.set	noreorder
	.set	nomacro
	bgez	$6,$L24
	addu	$7,$7,$2
	.set	macro
	.set	reorder

	addu	$8,$8,1
	slt	$2,$8,20
	sw	$7,0($10)
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L25
	addu	$9,$9,4
	.set	macro
	.set	reorder

	addu	$11,$11,1
	slt	$2,$11,20
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L39
	sll	$2,$11,2
	.set	macro
	.set	reorder

	la	$16,C+1596
	lw	$7,0($16)
	la	$4,$LC0
	li	$5,19			# 0x13
	.set	noreorder
	.set	nomacro
	jal	printf
	li	$6,19			# 0x13
	.set	macro
	.set	reorder

	lw	$2,0($16)
	lw	$31,20($sp)
	lw	$16,16($sp)
	#nop
	.set	noreorder
	.set	nomacro
	j	$31
	addu	$sp,$sp,24
	.set	macro
	.set	reorder

	.end	main

	.comm	A,1600

	.comm	B,1600

	.comm	C,1600
