	.file	1 "stdio.c"
	.text
	.align	2
	.globl	fgetc
	.ent	fgetc
fgetc:
	.frame	$sp,40,$31		# vars= 8, regs= 3/0, args= 16, extra= 0
	.mask	0x80030000,-8
	.fmask	0x00000000,0
	subu	$sp,$sp,40
	sw	$17,28($sp)
	sw	$16,24($sp)
	sw	$31,32($sp)
	move	$17,$4
	li	$16,1			# 0x1
	move	$4,$17
$L5:
	addu	$5,$sp,16
	.set	noreorder
	.set	nomacro
	jal	read
	li	$6,1			# 0x1
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	bne	$2,$16,$L5
	move	$4,$17
	.set	macro
	.set	reorder

	lbu	$2,16($sp)
	lw	$31,32($sp)
	lw	$17,28($sp)
	lw	$16,24($sp)
	#nop
	.set	noreorder
	.set	nomacro
	j	$31
	addu	$sp,$sp,40
	.set	macro
	.set	reorder

	.end	fgetc
	.align	2
	.globl	fputc
	.ent	fputc
fputc:
	.frame	$sp,32,$31		# vars= 8, regs= 1/0, args= 16, extra= 0
	.mask	0x80000000,-8
	.fmask	0x00000000,0
	subu	$sp,$sp,32
	move	$2,$4
	li	$6,1			# 0x1
	move	$4,$5
	addu	$5,$sp,16
	sw	$31,24($sp)
	.set	noreorder
	.set	nomacro
	jal	write
	sb	$2,16($sp)
	.set	macro
	.set	reorder

	lw	$31,24($sp)
	#nop
	.set	noreorder
	.set	nomacro
	j	$31
	addu	$sp,$sp,32
	.set	macro
	.set	reorder

	.end	fputc
	.align	2
	.globl	fputs
	.ent	fputs
fputs:
	.frame	$sp,32,$31		# vars= 0, regs= 3/0, args= 16, extra= 0
	.mask	0x80030000,-8
	.fmask	0x00000000,0
	subu	$sp,$sp,32
	sw	$31,24($sp)
	sw	$17,20($sp)
	sw	$16,16($sp)
	move	$17,$4
	.set	noreorder
	.set	nomacro
	jal	strlen
	move	$16,$5
	.set	macro
	.set	reorder

	move	$6,$2
	move	$4,$16
	.set	noreorder
	.set	nomacro
	jal	write
	move	$5,$17
	.set	macro
	.set	reorder

	lw	$31,24($sp)
	lw	$17,20($sp)
	lw	$16,16($sp)
	#nop
	.set	noreorder
	.set	nomacro
	j	$31
	addu	$sp,$sp,32
	.set	macro
	.set	reorder

	.end	fputs
