	.file	1 "strcpy.c"
	.text
	.align	2
	.globl	strcpy
	.ent	strcpy
strcpy:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, extra= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	move	$6,$4
$L2:
	lbu	$2,0($5)
	#nop
	sb	$2,0($4)
	lbu	$3,0($5)
	addu	$4,$4,1
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L2
	addu	$5,$5,1
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	j	$31
	move	$2,$6
	.set	macro
	.set	reorder

	.end	strcpy
