package nachos.filesys;

import java.util.Arrays;
import java.util.LinkedList;

import nachos.machine.Disk;
import nachos.machine.Lib;
import nachos.machine.Machine;

/**
 * INode contains detail information about a file.
 * Most important among these is the list of sector numbers the file occupied, 
 * it's necessary to find all the pieces of the file in the filesystem.
 * 
 * @author starforever
 */
public class INode
{
  /** represent a system file (free list) */
  public static final int TYPE_SYSTEM = 0;
  
  /** represent a folder */
  public static final int TYPE_FOLDER = 1;
  
  /** represent a normal file */
  public static final int TYPE_FILE = 2;
  
  /** represent a normal file that is marked as delete */
  public static final int TYPE_FILE_DEL = 3;
  
  /** represent a symbolic link file */
  public static final int TYPE_SYMLINK = 4;
  
  /** represent a folder that are not valid */
  public static final int TYPE_FOLDER_DEL = 5;
  
  public static final int INODE_CODE_SIZE = 32;
  
  /** size of the file in bytes */
  int file_size;
  
  /** the type of the file */
  int file_type;
  
  /** the number of programs that have access on the file */
  int use_count;
  
  /** the number of links on the file */
  int link_count;
  
  int inodeid;
  
  static LinkedList<INode> inodeList = new LinkedList<INode>();
  
  /** maintain all the sector numbers this file used in order */
  private LinkedList<Integer> sec_addr = new LinkedList<Integer>();
  
  /**
   * construct a INode for an empty file
   */
  public INode ()
  {
	  file_type = file_size = use_count = 0;
	  link_count = 1;
	  inodeid = inodeList.size();
	  inodeList.add(this);
  }
  
  /**
   * construct a INode from a specified sector, offset means that the 
   * index in this sector of INode that you want to create, offset start from 0
   * after construction, release all the resources occupied by the link pages
   * @param sectno
   * @param offset
   */
  /*public INode (int sectno, int offset)
  {
	  Lib.assertTrue(offset >= 0 && offset < 16);
	  byte[] sector = new byte[nachos.machine.Disk.SectorSize];
	  int addr;
	  nachos.machine.Machine.synchDisk().readSector(sectno, sector, 0);
	  file_type = Lib.bytesToInt(sector, 0 + 32 * offset, 1);
	  file_size = Lib.bytesToInt(sector, 1 + 32 * offset, 3);
	  use_count = Lib.bytesToInt(sector, 4 + 32 * offset, 1);
	  link_count = Lib.bytesToInt(sector, 5 + 32 * offset, 1);
	  sec_addr = new LinkedList<Integer>();
	  for (int i = 0; i < 12; ++i)
	  {
		  addr = Lib.bytesToInt(sector, 6 + 2 * i + 32 * offset, 2);
		  if (addr == 0)
			  break;
		  sec_addr.add(addr);
	  }
	  if (sec_addr.size() == 12)
	  {
		  addr = Lib.bytesToInt(sector, 30 + 32 * offset, 2);
		  nachos.machine.Machine.synchDisk().readSector(addr, sector, 0);
		  for (int i = 0; i < 240; ++i)
		  {
			  addr = Lib.bytesToInt(sector, 2 * i, 2);
			  if (addr == 0)
				  break;
			  sec_addr.add(addr);
		  }
		  if (sec_addr.size() == 252)
		  {
			  byte[] addrSector = new byte[nachos.machine.Disk.SectorSize];
			  for (int i = 240; i < 256; ++i)
			  {
				  addr = Lib.bytesToInt(sector, 2 * i, 2);
				  if (addr == 0)
					  break;
				  nachos.machine.Machine.synchDisk().readSector(addr, addrSector, 0);
				  for (int j = 0; j < 256; ++j)
				  {
					  addr = Lib.bytesToInt(addrSector, 2 * j, 2);
					  if (addr == 0)
						  break;
					  sec_addr.add(addr);
				  }
			  }
		  }
	  }
	  inodeid = inodeList.size();
	  inodeList.add(this);
  }*/
  
  static public INode getNthINode(int inodeid)
  {
	  if (inodeList.size() > inodeid)
		  return inodeList.get(inodeid);
	  return null;
  }
  
  /** get the nth sector number which the file used  */
  protected int getNthSector (int pos)
  {
    return this.sec_addr.get(pos);
  }
  
  /** add a new sector to the sector using list */
  protected void addNewSector(int sectno)
  {
	  this.sec_addr.add(sectno);
  }
  
  /** remove the last sector from the using list */
  protected void removeSector()
  {
	  this.sec_addr.removeLast();
  }
  
  /** change the file size and adjust the content in the inode accordingly */
  /*public void setFileSize (int size)
  {
    TODO implement this
  }*/
  
  /** free the disk space occupied by the file (including inode) */
  public void free ()
  {
    //TODO implement this
  }
  
  /** load inode content from the disk */
  public void load ()
  {
    //TODO implement this
  }
  
  /** save inode content to the disk */
  public void save ()
  {
	  byte[] sector = new byte[nachos.machine.Disk.SectorSize];
	  @SuppressWarnings("unchecked")
	  LinkedList<Integer> sec_addr = (LinkedList<Integer>) this.sec_addr.clone();
	  int addr;
	  Arrays.fill(sector, (byte)0);
	  Lib.bytesFromInt(sector, 0, 1, file_type);
	  Lib.bytesFromInt(sector, 1, 4, file_size);
	  Lib.bytesFromInt(sector, 4, 1, use_count);
	  Lib.bytesFromInt(sector, 5, 1, link_count);
	  for (int i = 0; i < 12; ++i)
	  {
		  if (sec_addr.isEmpty())
			  break;
		  addr = sec_addr.removeFirst();
		  Lib.bytesFromInt(sector, 6 + 2 * i, 2, addr);
	  }
	  //Lib.debug('f', "save!!");
	  if (!sec_addr.isEmpty())
	  {
		  int sectorAddr = FilesysKernel.realFileSystem.getFreeList().allocate();
		  Lib.bytesFromInt(sector, 30, 2, sectorAddr);
		  FilesysKernel.realFileSystem.fileOfINodes.write(inodeid * INODE_CODE_SIZE,
				  sector, 0, INODE_CODE_SIZE);
		  Arrays.fill(sector, (byte)0);
		  for (int i = 0; i < 240; ++i)
		  {
			  if (sec_addr.isEmpty())
				  break;
			  addr = sec_addr.removeFirst();
			  Lib.bytesFromInt(sector,2 * i, 2, addr);
		  }
		  if (!sec_addr.isEmpty())
		  {
			  byte[] addrSector = new byte[nachos.machine.Disk.SectorSize];
			  for (int i = 240; i < 256; ++i)
			  {
				  if (sec_addr.isEmpty())
					  break;
				  int linkpageAddr = FilesysKernel.realFileSystem.getFreeList().allocate();
				  Lib.bytesFromInt(sector,2 * i, 2, linkpageAddr);
				  Arrays.fill(addrSector, (byte)0);
				  for (int j = 0; j < 256; ++j)
				  {
					  if (sec_addr.isEmpty())
						  break;
					  addr = sec_addr.removeFirst();
					  Lib.bytesFromInt(addrSector,2 * j, 2, addr);
				  }
				  Machine.synchDisk().writeSector(linkpageAddr, addrSector, 0);
			  }
		  }
		  Machine.synchDisk().writeSector(sectorAddr, sector, 0);
	  }
	  else
		  FilesysKernel.realFileSystem.fileOfINodes.write(inodeid * INODE_CODE_SIZE,
				  sector, 0, INODE_CODE_SIZE);
  }
  
  public void setFileType(int type)
  {
	  this.file_type = type;
  }

  public void use()
  {
	  ++this.use_count;
  }
  
  public void unuse()
  {
	  --this.use_count;
  }
  
  public void link()
  {
	  ++this.link_count;
  }
  
  public void unlink()
  {
	  --this.link_count;
	  if (link_count == 0)
	  {
		  GlobalFile file = GlobalFile.getNewGlobalFile(this);
		  //System.err.println("unlink: size" + Lib.divRoundUp(file.length(), Disk.SectorSize));
		  //System.err.println("free size: " + FilesysKernel.realFileSystem.getFreeSize());
		  //System.err.flush();
		  if (file != null)
		  {
			  this.releaseLinkPage();
			  file.delete();
		  }
		  inodeList.set(this.inodeid, null);
	  }
  }
  
  private void releaseLinkPage()
  {
	  byte[] sector = new byte[nachos.machine.Disk.SectorSize];
	  FilesysKernel.realFileSystem.fileOfINodes.read(inodeid * INODE_CODE_SIZE,
			  sector, 0, INODE_CODE_SIZE);
	  int addr = Lib.bytesToInt(sector, 30, 2);
	  if (addr == 0)
		  return;
	  nachos.machine.Machine.synchDisk().readSector(addr, sector, 0);
	  for (int i = 240; i < 256; ++i)
	  {
		  int insideAddr = Lib.bytesToInt(sector, 2 * i, 2);
		  if (insideAddr == 0)
			  break;
		  FilesysKernel.realFileSystem.getFreeList().deallocate(insideAddr);
	  }
	  FilesysKernel.realFileSystem.getFreeList().deallocate(addr);
  }
}
