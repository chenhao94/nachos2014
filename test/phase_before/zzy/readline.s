	.file	1 "readline.c"
	.rdata
	.align	2
$LC0:
	.ascii	"\b \b\000"
	.text
	.align	2
	.globl	readline
	.ent	readline
readline:
	.frame	$sp,40,$31		# vars= 0, regs= 6/0, args= 16, extra= 0
	.mask	0x801f0000,-4
	.fmask	0x00000000,0
	subu	$sp,$sp,40
	sw	$20,32($sp)
	sw	$19,28($sp)
	sw	$18,24($sp)
	sw	$17,20($sp)
	sw	$16,16($sp)
	sw	$31,36($sp)
	move	$20,$4
	move	$19,$5
	move	$16,$0
	li	$18,10			# 0xa
	li	$17,8			# 0x8
$L14:
	.set	noreorder
	.set	nomacro
	jal	fgetc
	move	$4,$0
	.set	macro
	.set	reorder

	sll	$2,$2,24
	sra	$2,$2,24
	.set	noreorder
	.set	nomacro
	beq	$2,$18,$L16
	slt	$3,$2,32
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	beq	$2,$17,$L17
	li	$4,7			# 0x7
	.set	macro
	.set	reorder

	li	$5,1			# 0x1
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L15
	addu	$6,$16,1
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	beq	$6,$19,$L15
	addu	$3,$20,$16
	.set	macro
	.set	reorder

	sb	$2,0($3)
	move	$4,$2
	li	$5,1			# 0x1
	move	$16,$6
$L15:
	jal	fputc
	j	$L14
$L17:
	.set	noreorder
	.set	nomacro
	beq	$16,$0,$L15
	li	$5,1			# 0x1
	.set	macro
	.set	reorder

	la	$4,$LC0
	.set	noreorder
	.set	nomacro
	jal	printf
	addu	$16,$16,-1
	.set	macro
	.set	reorder

	j	$L14
$L16:
	li	$4,10			# 0xa
	.set	noreorder
	.set	nomacro
	jal	fputc
	li	$5,1			# 0x1
	.set	macro
	.set	reorder

	addu	$2,$20,$16
	lw	$31,36($sp)
	lw	$20,32($sp)
	lw	$19,28($sp)
	lw	$18,24($sp)
	lw	$17,20($sp)
	lw	$16,16($sp)
	sb	$0,0($2)
	.set	noreorder
	.set	nomacro
	j	$31
	addu	$sp,$sp,40
	.set	macro
	.set	reorder

	.end	readline
