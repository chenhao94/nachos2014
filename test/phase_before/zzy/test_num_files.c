#include "syscall.h"
#include "stdio.h"
#include "stdlib.h"

#define ASSERT_TRUE( expr, ...)	\
	do {	\
		if (!(expr)) {	\
			printf("assertion failure: %s(%d): ", __FILE__, __LINE__);	\
			printf(__VA_ARGS__);	\
			exit(1);	\
		}	\
	} while(0)

int main() {
	int i;
	
	int fd[17] = {0, 1};
	char buf[100];
	for (i = 2; i < 16; ++i) {
		sprintf(buf, "%d.tmp", i);
		ASSERT_TRUE( (fd[i] = creat(buf)) >= 0, "failed to creat %s\n", buf);
	}
	
	ASSERT_TRUE( (fd[16] = creat("must_fail.tmp")) < 0, "more than 16 files opened\n");
	
	for (i = 2; i < 16; ++i) {
		ASSERT_TRUE( !close(fd[i]), "close %d failed\n", fd[i]);
	}

	ASSERT_TRUE( !close(0), "close stdin failed\n", 0);
	ASSERT_TRUE( !close(1), "close stdout failed\n", 1);

	ASSERT_TRUE( write(1, "cannot write this\n", 18) < 0, "still can write to stdout?\n" );

	for (i = 0; i < 16; ++i) {
		sprintf(buf, "%d.tmp", i);
		fd[i] = creat(buf);
		if (fd[i] < 0) {
			sprintf(buf, "open %d.tmp failed");
			char* argv[] = {"echo.coff", buf };
			int pid = exec(argv[0], 2, argv);
			if (pid > 0) {
				join(pid, 0);
			}
			exit(1);
		}
	}

	fd[16] = creat("must_fail.tmp");
	if (fd[16] >= 0) {
			sprintf(buf, "more than 16 files opened");
			char* argv[] = {"echo.coff", buf };
			int pid = exec(argv[0], 2, argv);
			if (pid > 0) {
				join(pid, 0);
			}
			exit(1);
	}

	for (i = 0; i < 16; ++i) {
		sprintf(buf, "%d.tmp", i);
		int ret1 = close(fd[i]);
		int ret2 = unlink(buf);
		if (ret1 || ret2) {
			sprintf(buf, "error when closing or unlinking %d.tmp", i);
			char* argv[] = {"echo.coff", buf };
			int pid = exec(argv[0], 2, argv);
			if (pid > 0) {
				join(pid, 0);
			}
			exit(1);
		}
	}

			sprintf(buf, "success");
			char* argv[] = {"echo.coff", buf};
			int pid = exec(argv[0], 2, argv);
			if (pid > 0) {
				join(pid, 0);
			}

	return 0;
}
