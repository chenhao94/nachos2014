#Organization of freelist
Located in sector 0, use one sector to record whether a sector is free or not (bit map)
2^12 sector in total, can be stored exactly in 1 sector

#Organization of folder entry and file name
16 bytes per entry, which contains

* 12 bytes for the prefix of the file name
* 2 bytes for the address of remaining part of the file name(if have)
    * the ID in the file of name blocks, start from 1
* 2 bytes for the INode ID
	* first 12 bits for the sector number, 4 bits for the offset

#Organization of INode
32 bytes per INode, which contains

* 1 byte for the file type
* 3 bytes for the file size
* 1 byte for use count
* 1 byte for link count
* 24 byte for 12 addresses of data blocks (2 bytes per address) (6KB support)
* 2 byte for address of extended link sector
	in extened link sector, 512 bytes in total (256 addresses)
	* first 480 bytes for 240 addresses of data blocks (120KB support)
	* last 32 bytes for 16 addresses of sectors of links (support 128KB per address, 2MB in total)
The file contains all INode starts from sector 1

#The file contains all INode

* first entry is the inode of itself.
* second entry is the INode of file contains file name blocks.
* third is the INode of root directory.

#Organization of folder
Folder is a special type of file. It stores folder entries serially (16 bytes per entry).

* The first one is its parent folder (the parent of root is root itself)

#Organization of symbol link
A file contains one 2-byte integer only, which is the INode ID of target file
