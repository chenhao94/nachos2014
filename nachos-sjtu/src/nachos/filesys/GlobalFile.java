package nachos.filesys;

import java.util.Arrays;
import java.util.Collections;

import nachos.machine.Disk;
import nachos.machine.Lib;
import nachos.machine.Machine;
import nachos.threads.Lock;

public class GlobalFile
{
	INode inode;

	private int file_size;
	private java.util.ArrayList<Byte> dataCache;
	private java.util.BitSet dirty;
	private Lock lock;
	
	@Deprecated
	protected GlobalFile (INode inode)
	{
		this.inode = inode;
		file_size = inode.file_size;
		dataCache = new java.util.ArrayList<Byte>(Collections.nCopies(file_size, null));
		dirty = new java.util.BitSet();
		lock = new Lock();
	}
	
	static public GlobalFile getNewGlobalFile(INode inode)
	{
		GlobalFile file = FilesysKernel.realFileSystem.
				globalFiles.get(inode.inodeid);
		if (file == null)
		{
			file = new GlobalFile(inode);
			FilesysKernel.realFileSystem.
				globalFiles.put(inode.inodeid, file);
		}
		return file;
	}
  
	public int length ()
	{
		return file_size;
	}
    
	public int read (int pos, byte[] buffer, int start, int limit)
	{
		lock.acquire();
		try
		{
			int cnt = 0, sectst, secten;
			if (pos >= file_size || pos < 0)
				return -1;
			for (int i = 0; i < limit && pos < file_size; ++i, ++cnt, ++pos)
			{ 
				if (dataCache.get(pos) == null)
				{
					sectst = pos / Disk.SectorSize;
					secten = (pos + limit - 1) / Disk.SectorSize;
					moveIntoCache(sectst, secten);
				}
				buffer[start + i] = dataCache.get(pos).byteValue();
			}
			return cnt;
		}
		finally
		{
			lock.release();
		}
	}
	  
	/**
	 * write buffer to file
	 * 
	 * @param pos the position start to write in file
	 * @param buffer the context to write
	 * @param start the position start to get in buffer
	 * @param limit the length of written data
	 * @return
	 */
	public int write (int pos, byte[] buffer, int start, int limit)
	{
		lock.acquire();
		try
		{
			/*if (this.equals(FilesysKernel.realFileSystem.fileOfSwap))
				Lib.debug('f', "write on swap");
			else
			{
				int freesize = FilesysKernel.realFileSystem.getFreeSize();
				Lib.debug('f', "write: size " + Lib.divRoundUp(this.length(), Disk.SectorSize) + " on: " + this.toString());
				Lib.debug('f', "free size: " + freesize);
			}*/
	    	
			int cnt = 0, sectst, secten;
		    if (pos < 0)
		    	return -1;
		    if (pos + limit > file_size)
		    {	
		    	int oldSectTot = Lib.divRoundUp(file_size, Disk.SectorSize); 
				int newSectTot = Lib.divRoundUp(pos + limit, Disk.SectorSize);
				for (int i = oldSectTot; i < newSectTot; ++i)
				{
					int sectno = FilesysKernel.realFileSystem.getFreeList().allocate();
					if (sectno == -1)
						Lib.assertNotReached("no more free page to allocate!");
					this.inode.addNewSector(sectno);
				}
				dataCache.addAll(Collections.nCopies(pos + limit - file_size, null));
		    	file_size = pos + limit;
		    }
			for (int i = 0; i < limit && pos < file_size; ++i, ++cnt, ++pos)
			{ 
				if (dataCache.get(pos) == null)
				{
					sectst = pos / Disk.SectorSize;
					secten = (pos + limit - 1) / Disk.SectorSize;
					moveIntoCache(sectst, secten);
				}
				dataCache.set(pos, buffer[start + i]);
				dirty.set(pos / Disk.SectorSize);
			}
			return cnt;
		}
		finally
		{
			lock.release();
		}
	}
	
	public void setLength(int length)
	{
		lock.acquire();
		try
		{
			if (length == file_size)
				return;
			if (length < 0)
				length = 0;
			int oldSectTot = Lib.divRoundUp(file_size, Disk.SectorSize); 
			int newSectTot = Lib.divRoundUp(length, Disk.SectorSize);
			
			if (length > file_size)
			{
				for (; oldSectTot < newSectTot; ++oldSectTot)
				{
					int sectno = FilesysKernel.realFileSystem.getFreeList().allocate();
					if (sectno == -1)
						Lib.assertNotReached("no more free page to allocate!");
					inode.addNewSector(sectno);
				}
				dataCache.addAll(Collections.nCopies(length - file_size, null));
				file_size = length;
			}
			else
			{
				while (oldSectTot > newSectTot)
				{
					int sectno = inode.getNthSector(--oldSectTot);
					FilesysKernel.realFileSystem.getFreeList().deallocate(sectno);
					inode.removeSector();
				}
				while (file_size > length)
					dataCache.remove(--file_size);
			}
		}
		finally
		{
			lock.release();
		}
	}
	
	private void moveIntoCache(int sectst, int secten)
	{
		byte buffer[] = new byte[Disk.SectorSize];
		secten = Math.min(secten, Lib.divRoundUp(inode.file_size, Disk.SectorSize));
		
		for (int i = sectst; i < secten; ++i)
		{
			Machine.synchDisk().readSector(inode.getNthSector(i), buffer, 0);
			for (int j = 0; j < Disk.SectorSize; ++j)
				dataCache.set(i * Disk.SectorSize + j, buffer[j]);
		}
		
	}

	public void delete() 
	{
		setLength(0);
		
		/*Lib.debug(FilesysKernel.DEBUG_FLAG, "\ndelete");
		FilesysKernel.realFileSystem.getFreeSize();
		Lib.debug(FilesysKernel.DEBUG_FLAG, "\n");*/
		
		FilesysKernel.realFileSystem.globalFiles.remove(this.inode.inodeid);
	}
	
	public void save()
	{
		int oldSectNum = Lib.divRoundUp(inode.file_size, Disk.SectorSize);
		int newSectNum = Lib.divRoundUp(this.file_size, Disk.SectorSize);
		int cacheLen = newSectNum * Disk.SectorSize; 
		byte[] buffer, dataCache = new byte[cacheLen];
		
		for (int i = 0; i < cacheLen; ++i)
			if (i < this.dataCache.size())
				dataCache[i] = this.dataCache.get(i);
			else
				dataCache[i] = 0;
		
		for (int i = 0; i < newSectNum; ++i)
			if (i > oldSectNum || dirty.get(i))
			{
				buffer = Arrays.copyOfRange(dataCache, i * Disk.SectorSize, (i + 1) * Disk.SectorSize);
				Machine.synchDisk().writeSector(inode.getNthSector(i), buffer, 0);
			}
		dirty.clear();
		inode.file_size = this.file_size;
		inode.save();
	}
}
