package nachos.vm;

import nachos.machine.CoffSection;
import nachos.machine.Lib;
import nachos.machine.Machine;
import nachos.machine.Processor;
import nachos.machine.TranslationEntry;
import nachos.userprog.GlobalPageTable;
import nachos.userprog.UserProcess;

/**
 * A <tt>UserProcess</tt> that supports demand-paging.
 */
public class VMProcess extends UserProcess {
	/**
	 * Allocate a new process.
	 */
	public VMProcess() {
		super();
		myTLB = new TranslationEntry[TLBSize];
		for (int i = 0; i < TLBSize; ++i)
			myTLB[i] = new TranslationEntry();
	}

	/**
	 * Save the state of this process in preparation for a context switch.
	 * Called by <tt>UThread.saveState()</tt>.
	 */
	public void saveState() {
		//super.saveState();
		for (int i = 0 ; i< TLBSize; ++i)
		{
			TranslationEntry entry = myTLB[i] = Machine.processor().readTLBEntry(i);
			if (entry.valid){
				globalPageTable.updateEntry(pid, entry.vpn, entry.used, entry.dirty);
				Machine.processor().writeTLBEntry(i, new TranslationEntry());
			}
		}
		Lib.debug('x', "save state pid: " + pid);
	}

	/**
	 * Restore the state of this process after a context switch. Called by
	 * <tt>UThread.restoreState()</tt>.
	 */
	public void restoreState() {
		//super.restoreState();
		for (int i = 0 ; i< TLBSize; ++i)
		{	
			TranslationEntry entry = myTLB[i];
			if (entry.valid && !globalPageTable.own(pid, entry.vpn, entry.ppn))
				entry = new TranslationEntry();
			Machine.processor().writeTLBEntry(i, entry);
		}
		Lib.debug('x', "restore state pid: " + pid);
	}

	/**
	 * Initializes page tables for this process so that the executable can be
	 * demand-paged.
	 * 
	 * @return <tt>true</tt> if successful.
	 */
	protected boolean loadSections() {
		// load sections
		//globalPageTable.assign(pid, numPages);
		//globalPageTable.acquireLock();
		//for (int i = 0 ; i< TLBSize; ++i)
		//	Machine.processor().writeTLBEntry(i, new TranslationEntry());
		//try{
			//return super.loadSections();
			Lib.debug('x', "need pages num: " + numPages);
			
			for (int s = 0; s < coff.getNumSections(); s++) {
				CoffSection section = coff.getSection(s);
	
				Lib.debug(dbgProcess, "\tinitializing " + section.getName()
						+ " section (" + section.getLength() + " pages)");
	
				for (int i = 0; i < section.getLength(); i++) {
					int vpn = section.getFirstVPN() + i;
					globalPageTable.putSectionMap(pid, vpn, new SectionInfo(s, i, section));
					/*globalPageTable.setReadOnly(pid, vpn, section.isReadOnly());
					section.loadPage(i, globalPageTable.getPPN(pid, vpn, true));
					globalPageTable.setDirty(pid, vpn, true);
					Lib.debug('x', "Load pid: " + pid + ", vpn: " + vpn);*/
				}
			}
			return true;
		//}
		//finally{
		//	globalPageTable.releaseLock();
		//}
	}

	/**
	 * Release any resources allocated by <tt>loadSections()</tt>.
	 */
	protected void unloadSections() {
		//super.unloadSections();
		globalPageTable.release(pid);
		coff.close();		
	}

	/**
	 * Handle a user exception. Called by <tt>UserKernel.exceptionHandler()</tt>
	 * . The <i>cause</i> argument identifies which exception occurred; see the
	 * <tt>Processor.exceptionZZZ</tt> constants.
	 * 
	 * @param cause
	 *            the user exception that occurred.
	 */
	public void handleException(int cause) {
		//Lib.debug('h', "handleException: " + Processor.exceptionNames[cause]);
		Processor processor = Machine.processor();
		int badVaddr;

		switch (cause) {
		case Processor.exceptionTLBMiss:
			badVaddr = processor.readRegister(Processor.regBadVAddr);
			loadIntoTLB(getVPN(badVaddr));
			break;
		default:
			super.handleException(cause);
			break;
		}
	}
	
	@Override
	public int readVirtualMemory(int vaddr, byte[] data, int offset, int length){
		globalPageTable.acquireLock();
		int ret = super.readVirtualMemory(vaddr, data, offset, length);
		globalPageTable.releaseLock();
		return ret;
	}
	
	@Override
	public int writeVirtualMemory(int vaddr, byte[] data, int offset, int length) {
		globalPageTable.acquireLock();
		int ret = super.writeVirtualMemory(vaddr, data, offset, length);
		globalPageTable.releaseLock();
		return ret;
	}

	/** load virtual page into TLB
	 * 	using random strategy to replace TLB
	 * */
	private void loadIntoTLB(int vpn) {
		// TODO Auto-generated method stub
		int pos = Lib.random(TLBSize);
		Lib.debug('o', "TLBMiss on " + pid + ", " + vpn);
		TranslationEntry oldTLBEntry = Machine.processor().readTLBEntry(pos);
		
		//Machine.processor().writeTLBEntry(Lib.random(tlbSize), pageTable[vpn]);	
		if (oldTLBEntry.valid)
			globalPageTable.updateEntry(pid, oldTLBEntry.vpn, oldTLBEntry.used, oldTLBEntry.dirty);
		Machine.processor().writeTLBEntry(pos, globalPageTable.getEntry(pid, vpn));
	}
		
	public static GlobalPageTable getGlobalPageTable() {
		return globalPageTable;
	}
	
	@Deprecated
	public void invalidatePPN(int ppn) {
		for (TranslationEntry entry : myTLB)
			if (entry.ppn == ppn && entry.valid)
			{
				entry.valid = false;
				globalPageTable.updateEntry(pid, entry.vpn, entry.used, entry.dirty);
				break;
			}
	}
	
	private TranslationEntry[] myTLB; 
	
	private static final int TLBSize = nachos.machine.Machine.processor().getTLBSize();
	private static final char dbgProcess = 'a';
	//private static final char dbgVM = 'v';
}
