	.file	1 "strlen.c"
	.text
	.align	2
	.globl	strlen
	.ent	strlen
strlen:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, extra= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	lbu	$2,0($4)
	move	$3,$0
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L7
	addu	$4,$4,1
	.set	macro
	.set	reorder

$L5:
	lbu	$2,0($4)
	addu	$3,$3,1
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L5
	addu	$4,$4,1
	.set	macro
	.set	reorder

$L7:
	.set	noreorder
	.set	nomacro
	j	$31
	move	$2,$3
	.set	macro
	.set	reorder

	.end	strlen
