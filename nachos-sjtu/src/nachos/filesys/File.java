package nachos.filesys;

import nachos.machine.OpenFile;

/**
 * File provide some basic IO operations.
 * Each File is associated with an INode which stores the basic information for the file.
 * 
 * @author starforever
 */
public class File extends OpenFile
{
	GlobalFile globalFile;

	private int pos;
	private boolean open;

	public File (GlobalFile file, String name)
	{
		super(FilesysKernel.realFileSystem, name);
		globalFile = file;
		open = true;
		pos = 0;
	}
  
	public int length ()
	{
		return globalFile.length();
	}
  
	public void close ()
	{
		globalFile.save();
		globalFile.inode.unuse();
		open = false;
	}
	  
	public void seek (int pos)
	{
		this.pos = pos;
	}
	  
	public int tell ()
	{
		return pos;
	}
	  
	public int read (byte[] buffer, int start, int limit)
	{
		int ret = read(pos, buffer, start, limit);
	    pos += ret;
	    return ret;
	}
	  
	public int write (byte[] buffer, int start, int limit)
	{
		//Lib.debug('f', "write on :" + this.getName());
		int ret = write(pos, buffer, start, limit);
	    pos += ret;
	    return ret;
	}
	  
	public int read (int pos, byte[] buffer, int start, int limit)
	{
		if (!open)
			return -1;
		int cnt = globalFile.read(pos, buffer, start, limit);
		pos += cnt;
		return cnt;
	}
	  
	public int write (int pos, byte[] buffer, int start, int limit)
	{
		if (!open)
			return -1;
		int cnt = globalFile.write(pos, buffer, start, limit);
		pos += cnt;
		return cnt;
	}
	
	public void setLength(int length)
	{
		globalFile.setLength(length);
	}
}
