package nachos.filesys;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;

import nachos.machine.Disk;
import nachos.machine.FileSystem;
import nachos.machine.Lib;
import nachos.machine.Machine;
import nachos.machine.OpenFile;

/**
 * RealFileSystem provide necessary methods for filesystem syscall.
 * The FileSystem interface already define two basic methods, you should implement your own to adapt to your task.
 * 
 * @author starforever
 */
public class RealFileSystem implements FileSystem
{
  /** the free list */
  private FreeList free_list = new FreeList();
  
  /** the root folder */
  protected Folder root_folder;
  
  /** the current folder */
  protected Folder cur_folder;
  
  public GlobalFile fileOfINodes;
  public GlobalFile fileOfNameBlocks;
  public GlobalFile fileOfSwap;
  public HashMap<Integer, GlobalFile> globalFiles = 
		  new HashMap<Integer, GlobalFile>();
  
  /**
   * initialize the file system
   * 
   * @param format
   *          whether to format the file system
   */
  public void init (boolean format)
  {
	free_list.init(format);
    if (format)
    {
    	INode inodeOfInodes = new INode();
    	INode inodeOfNameBlocks = new INode();
    	INode inodeOfRoot = new INode();
    	inodeOfInodes.setFileType(INode.TYPE_FILE);
    	inodeOfNameBlocks.setFileType(INode.TYPE_FILE);
    	inodeOfRoot.setFileType(INode.TYPE_FOLDER);
    	Folder.createRoot(inodeOfRoot);
    	cur_folder = root_folder;
    	fileOfINodes = GlobalFile.getNewGlobalFile(inodeOfInodes);
    	fileOfNameBlocks = GlobalFile.getNewGlobalFile(inodeOfNameBlocks);
    	byte[] buffer = new byte[INode.INODE_CODE_SIZE * 100];
    	Arrays.fill(buffer, (byte)0);
    	fileOfINodes.write(0, buffer, 0, INode.INODE_CODE_SIZE * 100);
    	//Lib.debug(FilesysKernel.DEBUG_FLAG, "init for inode file.");
    	Lib.assertTrue(fileOfINodes.inode.getNthSector(0) == 1);
    	importStub();
    	FilesysProcess.getGlobalPageTable().openSwap();
    	fileOfSwap = root_folder.getFile("SWAP");
    }
    else
    {
      //TODO implement this
      /*
    	//INode inode_free_list = new INode(FreeList.STATIC_ADDR);
      inode_free_list.load();
      free_list = new FreeList(inode_free_list);
      free_list.load();
      
      //INode inode_root_folder = new INode(Folder.STATIC_ADDR);
      inode_root_folder.load();
      root_folder = new Folder(inode_root_folder);
      root_folder.load();*/
    	fileOfINodes = GlobalFile.getNewGlobalFile(INode.getNthINode(0));
        fileOfNameBlocks = GlobalFile.getNewGlobalFile(INode.getNthINode(1));
        root_folder = cur_folder = Folder.getExistingFolder(INode.getNthINode(2));
    }
  }
  
  public void finish ()
  {
    root_folder.save();
    free_list.save();
    fileOfNameBlocks.save();
    fileOfINodes.save();
  }
  
  /** import from stub filesystem */
  private void importStub ()
  {
    FileSystem stubFS = Machine.stubFileSystem();
    FileSystem realFS = FilesysKernel.realFileSystem;
    String[] file_list = Machine.stubFileList();
    for (int i = 0; i < file_list.length; ++i)
    {
      if (!file_list[i].endsWith(".coff"))
        continue;
      OpenFile src = stubFS.open(file_list[i], false);
      if (src == null)
      {
        continue;
      }
      OpenFile dst = realFS.open(file_list[i], true);
      int size = src.length();
      byte[] buffer = new byte[size];
      src.read(0, buffer, 0, size);
      dst.write(0, buffer, 0, size);
      src.close();
      dst.close();
    }
  }
  
  /** get the only free list of the file system */
  public FreeList getFreeList ()
  {
    return free_list;
  }
  
  /** get the only root folder of the file system */
  public Folder getRootFolder ()
  {
    return root_folder;
  }
  
  public Folder getFolder(String path)
  {
	  if (path == null || path.length() == 0)
		  return this.cur_folder;
	  if (path.equals("/"))
		  return this.root_folder;
	  String[] pathList = path.split("/");
	  String foldername;
	  int pos = 0, len = pathList.length;
	  Folder folder;
	  if (pathList[0].equals(""))
	  {
		  ++pos;
		  folder = this.root_folder;
	  }
	  else
		  folder = this.cur_folder;
	  for (; pos < len; ++pos)
	  {
		  foldername = pathList[pos];
		  if (foldername.equals("."))
			  continue;
		  else if (foldername.equals(".."))
			  folder = folder.getParent();
		  else
			  folder = folder.getSubDirectory(foldername);
	  }
	  return folder;
  }
  
  public LinkedList<String> getPath(Folder folder)
  {
	  Folder parent;
	  LinkedList<String> path = new LinkedList<String>();
	  while (true)
	  {
		  path.addFirst(folder.getName());
		  parent = folder.getParent();
		  if (parent.equals(folder))
			  break;
		  folder = parent;
	  }
	  return path;
  }
  
  public OpenFile open (String name, boolean create)
  {
	  String dirPath = null, filename;
	  int slashPos = name.lastIndexOf('/');
	  if (slashPos == -1)
		  filename = name;
	  else
	  {
		  dirPath = name.substring(0, slashPos + 1);
		  filename = name.substring(slashPos + 1);
	  }
	  Folder folder = getFolder(dirPath);
	  if (create)
		  return folder.create(filename);
	  else
		  return folder.open(filename, true);
  }
  
  public boolean remove (String name)
  {
	  try
	  {
		  String dirPath = null, filename;
		  int slashPos = name.lastIndexOf('/');
		  if (slashPos == -1)
			  filename = name;
		  else
		  {
			  dirPath = name.substring(0, slashPos + 1);
			  filename = name.substring(slashPos + 1);
		  }
		  getFolder(dirPath).remove(filename, false);
	  }
	  catch (Throwable e)
	  {
		  //System.err.println(e.getMessage());
		  return false;
	  }
	  return true;
  }
  
  public int createFolder (String name)
  {
	  try
	  {
		  String dirPath = null, filename;
		  int slashPos = name.lastIndexOf('/');
		  if (slashPos == -1)
			  filename = name;
		  else
		  {
			  dirPath = name.substring(0, slashPos + 1);
			  filename = name.substring(slashPos + 1);
		  }
		  return getFolder(dirPath).createFolder(filename);
	  }
	  catch (Throwable e)
	  {
		  //System.err.println(e.getMessage());
		  return -1;
	  }
  }
  
  public int removeFolder (String name)
  {
	  try
	  {
		  String dirPath = null, filename;
		  int slashPos = name.lastIndexOf('/');
		  if (slashPos == -1)
			  filename = name;
		  else
		  {
			  dirPath = name.substring(0, slashPos + 1);
			  filename = name.substring(slashPos + 1);
		  }
		  return getFolder(dirPath).removeFolder(filename, true);
	  }
	  catch (Throwable e)
	  {
		  //System.err.println(e.getMessage());
		  return -1;
	  }
  }
  
  public int changeCurFolder (String name)
  {
	  try
	  {
		  Folder tmpFolder = getFolder(name);
		  if (tmpFolder == null)
			  return -1;
		  this.cur_folder = tmpFolder;
	  }
	  catch (Throwable e)
	  {
		  //System.err.println(e.getMessage());
		  return -1;
	  }
	  return 0;
  }
  
  /*public String[] readDir (String name)
  {
    //TODO implement this
    return null;
  }
  
  public FileStat getStat (String name)
  {
    //TODO implement this
    return null;
  }*/
  
  /**
   *  create a link whose path is src, link to file dst
   * @param src
   * @param dst
   * @return
   */
  public boolean createLink (String src, String dst)
  {
	  try
	  {
		  GlobalFile dstFile = getFile(dst);
		  String srcPath = null, srcName;
		  int slashPos = src.lastIndexOf('/');
		  if (slashPos == -1)
			  srcName = src;
		  else
		  {
			  srcPath = src.substring(0, slashPos + 1);
			  srcName = src.substring(slashPos + 1);
		  }
		  getFolder(srcPath).createLink(srcName, dstFile);
	  }
	  catch (Throwable e)
	  {
		  //System.err.println(e.getMessage());
		  return false;
	  }
	  return true; 
  }
  
  public boolean createSymlink (String src, String dst)
  {
	  try
	  {
		  GlobalFile dstFile = getFile(dst);
		  String srcPath = null, srcName;
		  int slashPos = src.lastIndexOf('/');
		  if (slashPos == -1)
			  srcName = src;
		  else
		  {
			  srcPath = src.substring(0, slashPos + 1);
			  srcName = src.substring(slashPos + 1);
		  }
		  getFolder(srcPath).createSymlink(srcName, dstFile);
	  }
	  catch (Throwable e)
	  {
		  //System.err.println(e.getMessage());
		  return false;
	  }
	  return true; 
  }

  /**
   * get the global file located in given path
   */
  public GlobalFile getFile(String path)
  {
	  String dirPath = null, filename;
	  int slashPos = path.lastIndexOf('/');
	  if (slashPos == -1)
		  filename = path;
	  else
	  {
		  dirPath = path.substring(0, slashPos + 1);
		  filename = path.substring(slashPos + 1);
	  }
	  Folder folder = getFolder(dirPath);
	  return folder.getFile(filename);
  }
  
  /**
   * return the number of free sectors
   */
  public int getFreeSize()
  {
	//Lib.debug(FilesysKernel.DEBUG_FLAG, "freesize: " + free_list.sectorsRemain());
	return free_list.sectorsRemain();
  }
  
  public int getSwapFileSectors()
  {
	  //Lib.debug(FilesysKernel.DEBUG_FLAG, "swapsize: " + Lib.divRoundUp(this.root_folder.getFile("SWAP").length(), Disk.SectorSize));
    return Lib.divRoundUp(this.root_folder.getFile("SWAP").length(), Disk.SectorSize);
  }
}
