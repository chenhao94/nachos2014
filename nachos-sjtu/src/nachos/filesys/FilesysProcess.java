package nachos.filesys;

import java.util.Arrays;
import java.util.LinkedList;

import nachos.machine.Disk;
import nachos.machine.Lib;
import nachos.machine.Machine;
import nachos.machine.Processor;
import nachos.vm.VMProcess;

/**
 * FilesysProcess is used to handle syscall and exception through some callback methods.
 * 
 * @author starforever
 */
public class FilesysProcess extends VMProcess
{
  protected static final int SYSCALL_MKDIR = 14;
  protected static final int SYSCALL_RMDIR = 15;
  protected static final int SYSCALL_CHDIR = 16;
  protected static final int SYSCALL_GETCWD = 17;
  protected static final int SYSCALL_READDIR = 18;
  protected static final int SYSCALL_STAT = 19;
  protected static final int SYSCALL_LINK = 20;
  protected static final int SYSCALL_SYMLINK = 21;
  
  public int handleSyscall (int syscall, int a0, int a1, int a2, int a3)
  {
    switch (syscall)
    {
      case SYSCALL_MKDIR:
      {
    	  //this.filesysDebugInfo("mkdir");
    	  return handleMkdir(readVirtualMemoryString(a0));
      }
        
      case SYSCALL_RMDIR:
      {
    	  //this.filesysDebugInfo("rmdir");
    	  return handleRmdir(readVirtualMemoryString(a0));
      }
        
      case SYSCALL_CHDIR:
      {
    	  //this.filesysDebugInfo("chdir");
    	  return handleChdir(readVirtualMemoryString(a0));
      }
        
      case SYSCALL_GETCWD:
      {
    	  return handleGetcwd(a0, a1);
      }
        
      case SYSCALL_READDIR:
      {
    	  return handleReaddir(readVirtualMemoryString(a0), a1, a2, a3);
      }
        
      case SYSCALL_STAT:
      {
    	  return handleStat(readVirtualMemoryString(a0), a1);
      }
       
      case SYSCALL_LINK:
      {
    	  //this.filesysDebugInfo("link");
    	  return handleLink(readVirtualMemoryString(a0), readVirtualMemoryString(a1));
      }
      
      case SYSCALL_SYMLINK:
      {
    	  //this.filesysDebugInfo("symlink");
    	  int ret = handleSymLink(readVirtualMemoryString(a0), readVirtualMemoryString(a1));
    	  //this.filesysDebugInfo("symlink(after)");
    	  return ret;
      }
      
      default:
        return super.handleSyscall(syscall, a0, a1, a2, a3);
    }
  }
  
  public String getcwd()
  {
	  LinkedList<String> path = FilesysKernel.realFileSystem.getPath(FilesysKernel.realFileSystem.cur_folder);
	  String cwd = "";
	  for (String filename : path)
		  cwd += filename + "/";
	  return cwd;
  }
  
  public void filesysDebugInfo(String cmd)
  {
	  /*Lib.debug(FilesysKernel.DEBUG_FLAG, "----------------------");
	  Lib.debug(FilesysKernel.DEBUG_FLAG, cmd);
	  Lib.debug(FilesysKernel.DEBUG_FLAG, "----------------------");
	  Lib.debug(FilesysKernel.DEBUG_FLAG, getcwd());
	  for (FolderEntry entry : FilesysKernel.realFileSystem.cur_folder.getEntries())
	  {
		  int file_type = INode.getNthINode(entry.inodeID).file_type;
		  if (file_type == INode.TYPE_FOLDER)
			  Lib.debug(FilesysKernel.DEBUG_FLAG, entry.getName() + "/");
		  else if (file_type == INode.TYPE_SYMLINK)
			  Lib.debug(FilesysKernel.DEBUG_FLAG, entry.getName() + "/->");
		  else
			  Lib.debug(FilesysKernel.DEBUG_FLAG, entry.getName());
	  }*/
  }
  
  private int handleMkdir(String pathname) 
  {
	  return FilesysKernel.realFileSystem.createFolder(pathname);
  }

  private int handleRmdir(String pathname) 
  {
	  return FilesysKernel.realFileSystem.removeFolder(pathname);
  }

  private int handleChdir(String pathname) 
  {
	  return FilesysKernel.realFileSystem.changeCurFolder(pathname);
  }
  
  private int handleGetcwd(int buf_addr, int size) 
  {
	  LinkedList<String> cwd = FilesysKernel.realFileSystem.getPath(FilesysKernel.realFileSystem.cur_folder);
	  byte[] buffer = new byte[size];
	  int pos = 0, len = 0;
	  for (String filename : cwd)
	  {
		  len = filename.length();
		  if (pos + len + 2 >= size)
			  return -1;
		  System.arraycopy(filename.getBytes(), 0, buffer, pos, len);
		  pos += len;
		  buffer[pos++] = '/';
	  }
	  if (pos > 1)
		  buffer[--pos] = '\0';
	  buffer[pos++] = '\0';
	  if (this.writeVirtualMemory(buf_addr, buffer, 0, size) != size)
		  return -1;
	  return 0;
  }

  private int handleReaddir(String dirname, int buf_addr, int size, int namesize) 
  {
	  Folder cwd = FilesysKernel.realFileSystem.getFolder(dirname);
	  byte[] buffer = new byte[size * namesize];
	  String name;
	  LinkedList<FolderEntry> entries;
	  int pos = 0;
	  
	  if (cwd == null)
		  return -1;
	  entries = cwd.getEntries();
	  if (entries.size() >= size)
		  return -1;
	  Arrays.fill(buffer, (byte)0);
	  
	  for (FolderEntry entry :  entries)
	  {
		  if (entry.equals(entries.get(0)))
			  continue;
		  else
			  name = entry.getName();
		  if (name.length() >= namesize)
			  return -1;
		  System.arraycopy(name.toCharArray(), 0, buffer, (pos++) * namesize, name.length());
	  }
	  if (this.writeVirtualMemory(buf_addr, buffer, 0, size*namesize) != size*namesize)
		  return -1;
	  return 0;
  }
  
  private int handleStat(String filename, int stat_addr) 
  {
	  GlobalFile file = FilesysKernel.realFileSystem.getFile(filename);
	  if (file == null)
		  return -1;
	  INode inode = file.inode;
	  int type;
	  String name = filename.substring(filename.lastIndexOf('/'));
	  
	  switch (inode.file_type)
	  {
	  case INode.TYPE_FILE: type = FileStat.NORMAL_FILE_TYPE; break;
	  case INode.TYPE_FOLDER: type = FileStat.DIR_FILE_TYPE; break;
	  case INode.TYPE_SYMLINK: type = FileStat.LinkFileType; break;
	  default:
		  return -1;
	  }
	  byte[] code = (new FileStat(name, file.length(), 
			  Lib.divRoundUp(file.length(), Disk.SectorSize), 
			  type, inode.inodeid, inode.link_count)).encode();
	  if (this.writeVirtualMemory(stat_addr, code, 0, FileStat.STAT_SIZE) != FileStat.STAT_SIZE)
		  return -1;
	  return 0;
  }
  
  private int handleLink(String oldname, String newname)
  {
	  String dirPath = null, filename;
	  int slashPos = newname.lastIndexOf('/');
	  if (slashPos == -1)
		  filename = newname;
	  else
	  {
		  dirPath = newname.substring(0, slashPos + 1);
		  filename = newname.substring(slashPos + 1);
	  }
	  Folder folder = FilesysKernel.realFileSystem.getFolder(dirPath);
	  if (folder == null)
		  return -1;
	  return folder.createLink(filename, FilesysKernel.realFileSystem.getFile(oldname));
  }
  
  private int handleSymLink(String oldname, String newname)
  {
	  String dirPath = null, filename;
	  int slashPos = newname.lastIndexOf('/');
	  if (slashPos == -1)
		  filename = newname;
	  else
	  {
		  dirPath = newname.substring(0, slashPos + 1);
		  filename = newname.substring(slashPos + 1);
	  }
	  Folder folder = FilesysKernel.realFileSystem.getFolder(dirPath);
	  if (folder == null)
		  return -1;
	  return folder.createSymlink(filename, FilesysKernel.realFileSystem.getFile(oldname));
  }
	  
  public void handleException (int cause)
  {
	  Processor processor = Machine.processor();
	  if (cause == Processor.exceptionSyscall)
	  {
		  int result = handleSyscall(processor.readRegister(Processor.regV0),
				processor.readRegister(Processor.regA0), processor
						.readRegister(Processor.regA1), processor
						.readRegister(Processor.regA2), processor
						.readRegister(Processor.regA3));
		  processor.writeRegister(Processor.regV0, result);
		  processor.advancePC();
	  }
	  else
		  super.handleException(cause);
  }
}
