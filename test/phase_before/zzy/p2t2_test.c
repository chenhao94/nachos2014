#include "syscall.h"
#include "stdio.h"
#include "stdlib.h"

#define EXECUTE(pid, argv)	\
	int pid = exec(argv[0], sizeof(argv) / sizeof(char*), argv)

#define EXECUTE_AND_CHECK_PID(pid, argv)	\
	EXECUTE(pid, argv);	\
	if (pid < 0) {	\
		if (argv[0] != INVALID_ADDR) printf("failed to exec %s\n", argv[0]);	\
		else printf("argv[0] == INVALID_ADDR\n");	\
		break;	\
	}

#define ASSERT_TRUE( expr, ...)	\
	do {	\
		if (!(expr)) {	\
			printf("assertion failure: %s(%d): ", __FILE__, __LINE__);	\
			printf(__VA_ARGS__);	\
			exit(1);	\
		}	\
	} while(0)


#define JOIN_AND_CHECK(pid)	\
	do {	\
		int exitcode;	\
		int ret = join(pid, &exitcode);	\
		switch (ret) {	\
		case 1:	\
			printf("%d exited normally with %d\n", pid, exitcode);	\
			break;	\
		case 0:	\
			printf("%d exited because of unhandled exception %d\n", pid, exitcode);	\
			break;	\
		case -1:	\
			printf("%d is not my child?\n", pid);	\
			break;	\
		default:	\
			printf("join(%d, &exitcode) returns unknown value\n", pid, ret);	\
		}	\
	} while(0)
	
#define STATIC_ASSERT(cond)	\
	int __static_assert(int static_assertion_failure[][(cond) ? 1 : -1])

char* tmp_file = "p2t2_tmp_file";
unsigned magic_number = 0x12345678;

#define INVALID_ADDR ((void*) 0x80000000)

int main(int argc, char* argv[]) {
	if (argc == 1) {

		printf("Test 0: non-root halt, expected exitcode %d\n", 0x77);
		do {
			char* argv[] = { "halt.coff" };
			EXECUTE_AND_CHECK_PID(pid, argv);
			JOIN_AND_CHECK(pid);
		} while(0);
		printf("\n");

		printf("Test 1: halt, I'll halt as soon as forked returns\n");
		do {
			char* argv[] = { "p2t2_test.coff", "2" };
			EXECUTE_AND_CHECK_PID(pid, argv);
			JOIN_AND_CHECK(pid);
			printf("Test 1 cont., halting\n");
			halt();
			ASSERT_TRUE(0, "not reached");
		} while(0);		
		printf("\n");
	}
	
	else if (argc == 2) {
		printf("\n");
		printf("Where's test 2? It's eaten by the author.^v^\n");

		printf("\n");
		printf("Test 3: argument passing by exec, expecting 2, got %s\n", argv[1]);

		printf("\n");
		printf("Test 4: more argument passed by exec, echo abd acd afk\n");
		do {
			char* argv[] = { "echo.coff", "abd", "acd", "afk" };
			EXECUTE_AND_CHECK_PID(pid, argv);
			JOIN_AND_CHECK(pid);
		} while(0);

		printf("\n");
		printf("Test 5: error in exec arguments\n");
		do {
			char* argv[] = { "non-existent-file.coff" };
			EXECUTE_AND_CHECK_PID(pid, argv);
		} while(0);

		do {
			printf("exec INVALID_ADDR\n");
			char* argv[] = { INVALID_ADDR };
			EXECUTE_AND_CHECK_PID(pid, argv);
		} while(0);

		do {
			printf("cat.coff with argument 1 set to INVALID_ADDR\n");
			char* argv[] = {"cat.coff", INVALID_ADDR };
			EXECUTE_AND_CHECK_PID(pid, argv);
			ASSERT_TRUE( pid < 0, "exec \"cat.coff\" 0 should fail\n");
		} while(0);

		do {
			printf("echo.coff with argument 3 set to INVALID_ADDR\n");
			char* argv[] = {"echo.coff", "1", "2", INVALID_ADDR};
			EXECUTE_AND_CHECK_PID(pid, argv);
		} while(0);
		
		do {
			printf("unexecutable file p2t2_test.c\n");
			char* argv[] = { "p2t2_test.c" };
			EXECUTE_AND_CHECK_PID(pid, argv);
		} while(0);
	
		printf("\n");
		printf("Test 6: join tests\n");
		do {
			printf("join invalid pid\n");
			JOIN_AND_CHECK(123123);
		} while(0);

		do {
			printf("join one process twice\n");
			char* argv[] = {"echo.coff", "1"};
			EXECUTE_AND_CHECK_PID(pid, argv);
			JOIN_AND_CHECK(pid);
			JOIN_AND_CHECK(pid);
		} while(0);

		do {
			printf("join a crashed process\n");
			char* argv[] = { "badaddr.coff" };
			EXECUTE_AND_CHECK_PID(pid, argv);
			JOIN_AND_CHECK(pid);
		} while(0);

		
		printf("\n");
		printf("Test 7: file system test, make sure that p2t2_tmp_file does not exist\n");
		{
			
			unsigned x;
			STATIC_ASSERT( sizeof(x) == sizeof(magic_number) );
			int fd = open(tmp_file);
			ASSERT_TRUE( fd < 0, "delete %s before run this test\n", tmp_file);

			fd = creat(tmp_file);
			ASSERT_TRUE( fd >= 0, "failed to create %s", tmp_file);

			/* use write instead of fprintf */
			ASSERT_TRUE( write(fd, (void*) &magic_number, sizeof(magic_number)) == sizeof(magic_number), "write failed\n" );
			ASSERT_TRUE( !close(fd), "close failed\n");
	
			/* open the file and check the content against magic_number */
			ASSERT_TRUE( (fd = open(tmp_file)) >= 0, "failed to open %s\n", tmp_file);
			ASSERT_TRUE( (read(fd, (void*) &x, sizeof(x))) == sizeof(x), "failed to read the magic number\n");
			ASSERT_TRUE( x == magic_number, "incorrect magic number\n");
			ASSERT_TRUE( !close(fd), "closed failed\n");
			
			/* re-create it */
			ASSERT_TRUE( (fd = creat(tmp_file)) >= 0, "failed to create %s again\n", tmp_file);
			ASSERT_TRUE( !close(fd), "close failed\n");
		
		
			/* open the file; the file should have been truncated */
			ASSERT_TRUE( (fd = open(tmp_file)) >= 0, "failed to open %s\n", tmp_file);
			ASSERT_TRUE( (read(fd, (void*) &x, sizeof(x))) != sizeof(x), "the file is not truncated\n");
			
			/* second open of the file */
			int fd2 = open(tmp_file);
			int can_reopen;
			if (fd2 < 0) {
				printf("double open is rejected\n");
				can_reopen = 0;
			}
			else {
				printf("double open is allowed\n");
				can_reopen = 1;
				ASSERT_TRUE( !close(fd2), "close fd2 failed\n");
				ASSERT_TRUE( close(fd2), "re-close fd2 returns 0\n");
			}
			
			/* test unlink behaviour */
			ASSERT_TRUE( !unlink(tmp_file), "unlink %s failed\n", tmp_file);
			
			if (can_reopen) {
				ASSERT_TRUE( (fd2 = open(tmp_file)) < 0, "opened an unlinked file\n");
			}
			
			ASSERT_TRUE( !close(fd), "close failed\n");
			ASSERT_TRUE( (fd2 = open(tmp_file)) < 0, "opened an unlinked file\n");
			
			ASSERT_TRUE( (fd = creat(tmp_file)) >= 0, "failed to re-create the tmp file\n");
			
			printf("fork myself with 2 arguments 3 0 to test unlink behaviour\n");
			do {
				char* argv[] = { "p2t2_test.coff", "3", "0" };
				EXECUTE_AND_CHECK_PID(pid, argv);
				JOIN_AND_CHECK(pid);
			} while (0);
			
			/* close the file, and the file should be removed on close */
			ASSERT_TRUE( !close(fd), "close failed");
			
			printf("fork myself with 2 arguments 3 1 to test unlink behaviour\n");
			do {
				char* argv[] = { "p2t2_test.coff", "3", "1" };
				EXECUTE_AND_CHECK_PID(pid, argv);
				JOIN_AND_CHECK(pid);
			} while (0);
		}
		
		printf("p2t2 1 done\n");
	}
	
	else if (argc == 3) {
		if (argv[2][0] == '0') {
			ASSERT_TRUE( !unlink(tmp_file), "unlink failed\n");
			exit(0);
		}
		
		else {
			ASSERT_TRUE( unlink(tmp_file), "unlinked an non-existent file\n");
			ASSERT_TRUE( (open(tmp_file)) < 0, "opened an non-existent file\n");
			
			exit(0);
		}
	}
	
	else {
		assert(0 /* not reached */);
	}
	
	return 0;
}
