package nachos.filesys;

import java.util.Arrays;

import nachos.machine.Lib;

/** 
 * FolderEntry contains information used by Folder to map from filename to address of the file
 * 
 * @author starforever
 * */
class FolderEntry
{
	static public final int ENTRY_CODE_SIZE = 16;
	/** the file name */
	String name;
	byte[] namePrefix = new byte[12];
	int remainAddr;
	
	/** the sector number of the inode */
	int inodeID;
	
	public FolderEntry() {}
	
	public FolderEntry(String filename, INode inode)
	{
		name = filename;
		inodeID = inode.inodeid;
		if (filename.length() > 12)
		{
			byte[] remainName = new byte[256];
			Arrays.fill(remainName, (byte)0);
			System.arraycopy(filename.getBytes(), 12, remainName, 0, filename.length() - 12);
			GlobalFile fileOfNameBlocks = FilesysKernel.realFileSystem.fileOfNameBlocks;
			remainAddr = fileOfNameBlocks.length() / 256 + 1;
			fileOfNameBlocks.write((remainAddr - 1) * 256, remainName, 0, 256);
		}
		System.arraycopy(filename.getBytes(), 0, namePrefix, 0, Math.min(12, filename.length()));
	}
	
	byte[] encode()
	{
		byte[] code = new byte[ENTRY_CODE_SIZE];
		System.arraycopy(namePrefix, 0, code, 0, 12);
		System.arraycopy(Lib.bytesFromInt(remainAddr), 0, code, 12, 2);
		System.arraycopy(Lib.bytesFromInt(inodeID), 0, code, 14, 2);
		return code;
	}
	
	static FolderEntry decode(byte[] code)
	{
		FolderEntry entry = new FolderEntry();
		System.arraycopy(code, 0, entry.namePrefix, 0, 12);
		System.arraycopy(code, 0, Lib.bytesFromInt(entry.remainAddr), 12, 2);
		System.arraycopy(code, 0, Lib.bytesFromInt(entry.inodeID), 14, 2);
		
		byte[] filename = new byte[256];
		System.arraycopy(entry.namePrefix, 0, filename, 0, 12);
		if (entry.remainAddr != 0)
			FilesysKernel.realFileSystem.fileOfNameBlocks.read(256 * (entry.remainAddr - 1), filename, 12, 244);
		entry.name = new String(filename);
		
		return entry;
	}
	
	String getName()
	{
		return name;
	}
}
