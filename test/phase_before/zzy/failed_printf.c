#include "stdio.h"

static char* help_str = 
"This is a very long string (length > 256). This will lead to printf's failure for its\n\
 buffer size is limited to 256. This is not a good restriction. Rather than pop out an \n\
assertion failure, it's better to dynamically increase the buffer size. And I don't know\n\
 how to increase the length of this string. miaow.miaow.miaow.oooooooooooooooooooooooooo\n\
 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo\n\
 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo\n";

int main() {
	printf("%s\n", help_str);
}
