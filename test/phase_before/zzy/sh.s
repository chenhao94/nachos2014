	.file	1 "sh.c"
	.rdata
	.align	2
$LC2:
	.ascii	"Unmatched \".\n\000"
	.align	2
$LC1:
	.ascii	"Unmatched \\.\n\000"
	.align	2
$LC0:
	.ascii	"sh.c\000"
	.text
	.align	2
	.ent	tokenizeCommand
tokenizeCommand:
	.frame	$sp,48,$31		# vars= 0, regs= 7/0, args= 16, extra= 0
	.mask	0x803f0000,-8
	.fmask	0x00000000,0
	subu	$sp,$sp,48
	sw	$21,36($sp)
	sw	$20,32($sp)
	sw	$19,28($sp)
	sw	$18,24($sp)
	sw	$17,20($sp)
	sw	$16,16($sp)
	sw	$31,40($sp)
	move	$21,$5
	move	$16,$4
	move	$19,$6
	move	$17,$7
	move	$18,$0
	.set	noreorder
	.set	nomacro
	blez	$5,$L48
	move	$20,$0
	.set	macro
	.set	reorder

$L3:
	lb	$3,0($16)
	#nop
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L41
	addu	$16,$16,1
	.set	macro
	.set	reorder

	move	$6,$19
	move	$4,$0
	li	$7,110			# 0x6e
	li	$8,116			# 0x74
	li	$5,-2			# 0xfffffffffffffffe
	li	$9,34			# 0x22
	li	$11,92			# 0x5c
	li	$13,-3			# 0xfffffffffffffffd
	li	$14,32			# 0x20
	li	$15,-5			# 0xfffffffffffffffb
	li	$10,34			# 0x22
	li	$12,92			# 0x5c
$L35:
	.set	noreorder
	.set	nomacro
	beq	$4,$0,$L7
	andi	$2,$18,0x2
	.set	macro
	.set	reorder

	beq	$3,$7,$L10
	beq	$3,$8,$L49
$L8:
	and	$18,$18,$5
$L32:
	sb	$3,0($17)
$L44:
	andi	$4,$18,0x1
$L45:
	addu	$17,$17,1
$L4:
	lb	$3,0($16)
	#nop
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L35
	addu	$16,$16,1
	.set	macro
	.set	reorder

$L40:
	bne	$4,$0,$L50
	andi	$2,$18,0x2
	bne	$2,$0,$L51
	andi	$2,$18,0x4
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L1
	move	$2,$20
	.set	macro
	.set	reorder

	sb	$0,0($17)
$L1:
	lw	$31,40($sp)
	lw	$21,36($sp)
	lw	$20,32($sp)
	lw	$19,28($sp)
	lw	$18,24($sp)
	lw	$17,20($sp)
	lw	$16,16($sp)
	#nop
	.set	noreorder
	.set	nomacro
	j	$31
	addu	$sp,$sp,48
	.set	macro
	.set	reorder

$L51:
	la	$4,$LC2
$L47:
	jal	printf
$L42:
	.set	noreorder
	.set	nomacro
	j	$L1
	li	$2,-1			# 0xffffffffffffffff
	.set	macro
	.set	reorder

$L50:
	la	$4,$LC1
	j	$L47
$L49:
	.set	noreorder
	.set	nomacro
	j	$L8
	li	$3,9			# 0x9
	.set	macro
	.set	reorder

$L10:
	.set	noreorder
	.set	nomacro
	j	$L8
	li	$3,10			# 0xa
	.set	macro
	.set	reorder

$L7:
	beq	$2,$0,$L14
	beq	$3,$9,$L17
	beq	$3,$11,$L46
	.set	noreorder
	.set	nomacro
	j	$L45
	sb	$3,0($17)
	.set	macro
	.set	reorder

$L46:
	ori	$18,$18,0x1
$L43:
$L53:
	.set	noreorder
	.set	nomacro
	j	$L4
	andi	$4,$18,0x1
	.set	macro
	.set	reorder

$L17:
	.set	noreorder
	.set	nomacro
	j	$L43
	and	$18,$18,$13
	.set	macro
	.set	reorder

$L14:
	slt	$2,$3,9
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L54
	andi	$2,$18,0x4
	.set	macro
	.set	reorder

	slt	$2,$3,11
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L55
	andi	$2,$18,0x4
	.set	macro
	.set	reorder

	beq	$3,$14,$L55
$L54:
	bne	$2,$0,$L27
	.set	noreorder
	.set	nomacro
	beq	$20,$21,$L42
	addu	$20,$20,1
	.set	macro
	.set	reorder

	sw	$17,0($6)
	addu	$6,$6,4
	ori	$18,$18,0x4
$L27:
	beq	$3,$10,$L31
	bne	$3,$12,$L32
	.set	noreorder
	.set	nomacro
	j	$L53
	ori	$18,$18,0x1
	.set	macro
	.set	reorder

$L31:
	.set	noreorder
	.set	nomacro
	j	$L43
	ori	$18,$18,0x2
	.set	macro
	.set	reorder

$L55:
	beq	$2,$0,$L4
	and	$18,$18,$15
	.set	noreorder
	.set	nomacro
	j	$L44
	sb	$0,0($17)
	.set	macro
	.set	reorder

$L41:
	.set	noreorder
	.set	nomacro
	j	$L40
	move	$4,$0
	.set	macro
	.set	reorder

$L48:
	la	$4,$LC0
	.set	noreorder
	.set	nomacro
	jal	__assert
	li	$5,32			# 0x20
	.set	macro
	.set	reorder

	j	$L3
	.end	tokenizeCommand
	.rdata
	.align	2
$LC3:
	.ascii	"&\000"
	.align	2
$LC4:
	.ascii	"exit\000"
	.align	2
$LC5:
	.ascii	"exit: Expression Syntax.\n\000"
	.align	2
$LC15:
	.ascii	"\n"
	.ascii	"[%d] Done (%d)\n\000"
	.align	2
$LC13:
	.ascii	"join: Invalid process ID.\n\000"
	.align	2
$LC14:
	.ascii	"\n"
	.ascii	"[%d] Unhandled exception\n\000"
	.align	2
$LC16:
	.ascii	"\n"
	.ascii	"[%d]\n\000"
	.align	2
$LC6:
	.ascii	"halt\000"
	.align	2
$LC8:
	.ascii	"halt: Expression Syntax.\n\000"
	.align	2
$LC7:
	.ascii	"Not the root process!\n\000"
	.align	2
$LC9:
	.ascii	"join\000"
	.align	2
$LC10:
	.ascii	"join: Expression Syntax.\n\000"
	.align	2
$LC11:
	.ascii	".coff\000"
	.align	2
$LC12:
	.ascii	"%s: exec failed.\n\000"
	.text
	.align	2
	.globl	runline
	.ent	runline
runline:
	.frame	$sp,240,$31		# vars= 200, regs= 5/0, args= 16, extra= 0
	.mask	0x800f0000,-8
	.fmask	0x00000000,0
	subu	$sp,$sp,240
	sw	$18,224($sp)
	addu	$18,$sp,144
	li	$5,16			# 0x10
	move	$6,$18
	addu	$7,$sp,16
	sw	$17,220($sp)
	sw	$31,232($sp)
	sw	$19,228($sp)
	.set	noreorder
	.set	nomacro
	jal	tokenizeCommand
	sw	$16,216($sp)
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	blez	$2,$L56
	move	$17,$2
	.set	macro
	.set	reorder

	sll	$2,$2,2
	addu	$3,$sp,$2
	lw	$4,140($3)
	la	$5,$LC3
	jal	strcmp
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L59
	move	$19,$0
	.set	macro
	.set	reorder

	addu	$17,$17,-1
	li	$19,1			# 0x1
$L59:
	blez	$17,$L56
	lw	$4,144($sp)
	la	$5,$LC4
	jal	strcmp
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L61
	li	$2,1			# 0x1
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	beq	$17,$2,$L86
	move	$4,$0
	.set	macro
	.set	reorder

	li	$2,2			# 0x2
	beq	$17,$2,$L90
	la	$4,$LC5
$L89:
	jal	printf
$L56:
	lw	$31,232($sp)
	lw	$19,228($sp)
	lw	$18,224($sp)
	lw	$17,220($sp)
	lw	$16,216($sp)
	#nop
	.set	noreorder
	.set	nomacro
	j	$31
	addu	$sp,$sp,240
	.set	macro
	.set	reorder

$L90:
	lw	$4,148($sp)
	jal	atoi
	move	$4,$2
$L86:
	jal	exit
$L66:
	bne	$19,$0,$L76
	move	$4,$16
	.set	noreorder
	.set	nomacro
	jal	join
	addu	$5,$sp,208
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L79
	move	$3,$2
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	blez	$2,$L91
	li	$2,1			# 0x1
	.set	macro
	.set	reorder

	bne	$3,$2,$L56
	lw	$6,208($sp)
	la	$4,$LC15
	.set	noreorder
	.set	nomacro
	jal	printf
	move	$5,$16
	.set	macro
	.set	reorder

	j	$L56
$L91:
	li	$2,-1			# 0xffffffffffffffff
	bne	$3,$2,$L56
	la	$4,$LC13
	j	$L89
$L79:
	la	$4,$LC14
	move	$5,$16
$L88:
	jal	printf
	j	$L56
$L76:
	la	$4,$LC16
	.set	noreorder
	.set	nomacro
	j	$L88
	move	$5,$16
	.set	macro
	.set	reorder

$L61:
	lw	$4,144($sp)
	la	$5,$LC6
	jal	strcmp
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L67
	li	$2,1			# 0x1
	.set	macro
	.set	reorder

	beq	$17,$2,$L92
	la	$4,$LC8
	j	$L89
$L92:
	jal	halt
	la	$4,$LC7
	j	$L89
$L67:
	lw	$4,144($sp)
	la	$5,$LC9
	jal	strcmp
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L71
	li	$2,2			# 0x2
	.set	macro
	.set	reorder

	beq	$17,$2,$L93
	la	$4,$LC10
	j	$L89
$L93:
	lw	$4,148($sp)
	jal	atoi
	.set	noreorder
	.set	nomacro
	j	$L66
	move	$16,$2
	.set	macro
	.set	reorder

$L71:
	addu	$16,$sp,80
	lw	$5,144($sp)
	.set	noreorder
	.set	nomacro
	jal	strcpy
	move	$4,$16
	.set	macro
	.set	reorder

	la	$5,$LC11
	.set	noreorder
	.set	nomacro
	jal	strcat
	move	$4,$16
	.set	macro
	.set	reorder

	move	$4,$16
	move	$5,$17
	.set	noreorder
	.set	nomacro
	jal	exec
	move	$6,$18
	.set	macro
	.set	reorder

	move	$16,$2
	li	$2,-1			# 0xffffffffffffffff
	bne	$16,$2,$L66
	lw	$5,144($sp)
	la	$4,$LC12
	j	$L88
	.end	runline
	.rdata
	.align	2
$LC17:
	.ascii	"nachos% \000"
	.align	2
$LC18:
	.ascii	"%s\000"
	.text
	.align	2
	.globl	main
	.ent	main
main:
	.frame	$sp,104,$31		# vars= 80, regs= 2/0, args= 16, extra= 0
	.mask	0x80010000,-4
	.fmask	0x00000000,0
	subu	$sp,$sp,104
	sw	$16,96($sp)
	sw	$31,100($sp)
	jal	__main
	la	$2,$LC17
	lbu	$5,8($2)
	lw	$3,0($2)
	lw	$4,4($2)
	sw	$3,16($sp)
	sw	$4,20($sp)
	sb	$5,24($sp)
	addu	$16,$sp,32
$L98:
	la	$4,$LC18
	.set	noreorder
	.set	nomacro
	jal	printf
	addu	$5,$sp,16
	.set	macro
	.set	reorder

	move	$4,$16
	.set	noreorder
	.set	nomacro
	jal	readline
	li	$5,64			# 0x40
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	jal	runline
	move	$4,$16
	.set	macro
	.set	reorder

	j	$L98
	.end	main
