package nachos.filesys;

import nachos.machine.Lib;

public class FileStat
{
	public FileStat(String name, int size, int sectors, int type, int inode,
			int links) 
	{
		this.name = name;
		this.size = size;
		this.sectors = sectors;
		this.type = type;
		this.inode = inode;
		this.links = links;
	}
	
	public byte[] encode()
	{
		byte[] code = new byte[STAT_SIZE];
		System.arraycopy(name.toCharArray(), 0, code, 0, name.length());
		System.arraycopy(Lib.bytesFromInt(size), 0, code, FILE_NAME_MAX_LEN , 4);
		System.arraycopy(Lib.bytesFromInt(sectors), 0, code, FILE_NAME_MAX_LEN + 4, 4);
		System.arraycopy(Lib.bytesFromInt(type), 0, code, FILE_NAME_MAX_LEN + 8, 4);
		System.arraycopy(Lib.bytesFromInt(inode), 0, code, FILE_NAME_MAX_LEN + 12, 4);
		System.arraycopy(Lib.bytesFromInt(links), 0, code, FILE_NAME_MAX_LEN + 16, 4);
		return code;
	}
  
  public static final int FILE_NAME_MAX_LEN = 256;
  public static final int STAT_SIZE = FILE_NAME_MAX_LEN + 20;
  public static final int NORMAL_FILE_TYPE = 0;
  public static final int DIR_FILE_TYPE = 1;
  public static final int LinkFileType = 2;
  
  public String name;
  public int size;
  public int sectors;
  public int type;
  public int inode;
  public int links;
}
