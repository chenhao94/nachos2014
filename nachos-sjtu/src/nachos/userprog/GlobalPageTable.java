package nachos.userprog;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import nachos.machine.CoffSection;
import nachos.machine.Lib;
import nachos.machine.OpenFile;
import nachos.machine.Processor;
import nachos.machine.TranslationEntry;
import nachos.vm.SectionInfo;

public class GlobalPageTable {
	GlobalPageTable() {}
	
	public int getPPN(int pid, int vpn, boolean write) {
		TranslationEntry entry = getEntry(pid, vpn);
		entry.used = true;
		entry.dirty |= write;
		return entry.ppn;
	}

	public void setReadOnly(int pid, int vpn, boolean readOnly) {
		TranslationEntry entry = getEntry(pid, vpn);
		boolean needReleaseLock = false;
		if (!gptlock.isHeldByCurrentThread())
		{
			gptlock.acquire();
			needReleaseLock = true;
		}
		entry.readOnly = readOnly;
		if (needReleaseLock)
			gptlock.release();
	}

	public void setDirty(int pid, int vpn, boolean dirty) {
		TranslationEntry entry = getEntry(pid, vpn);
		boolean needReleaseLock = false;
		if (!gptlock.isHeldByCurrentThread())
		{
			gptlock.acquire();
			needReleaseLock = true;
		}
		entry.dirty = dirty;
		if (needReleaseLock)
			gptlock.release();
	}
	
	public void updateEntry(int pid, int vpn, boolean used, boolean dirty){
		TranslationEntry entry = globalPageTable.get(new VPNInfo(pid, vpn));
		if (entry == null)
			System.err.println("Update Entry Error: " + pid + ": " + vpn);
		entry.used |= used;
		entry.dirty |= dirty;
		globalPageTable.put(new VPNInfo(pid, vpn), entry);
	}
	
	public void put(int pid, int vpn, TranslationEntry entry) {
		put(new VPNInfo(pid, vpn), entry);
	}
	
	public void put(VPNInfo vpn, TranslationEntry entry) {
		boolean needReleaseLock = false;
		if (!gptlock.isHeldByCurrentThread())
		{
			gptlock.acquire();
			needReleaseLock = true;
		}
		TranslationEntry oldEntry =  globalPageTable.get(vpn);
		if (oldEntry != null){
			entry.used |= oldEntry.used;
			entry.dirty |= oldEntry.dirty;
		}
		
		globalPageTable.put(vpn, entry);
		PhysPagesMap.put(entry.ppn, vpn);
		if (needReleaseLock)
			gptlock.release();
	}
	
	/**
	 * release all the page table allocated to process pid,
	 * put them back to available, and remove from global page table;
	 * 
	 * need not to acquire gptlock, because another lock (pageLock) ensure
	 * the atomicity
	 * @param pid
	 */
	public void releasePages(int pid){
		UserKernel.pageLock.acquire();
		Iterator<Entry<VPNInfo, TranslationEntry>> itr = 
				globalPageTable.entrySet().iterator();
		while (itr.hasNext()){
			Map.Entry<VPNInfo, TranslationEntry> e = 
					(Map.Entry<VPNInfo, TranslationEntry>)itr.next();
			if (e.getKey().pid == pid)
				UserKernel.availablePages.add(e.getValue().ppn);
		}
		Lib.debug('x', "PageNum remain: " + UserKernel.availablePages.size());
		Lib.debug('x', "Global page table size: " + globalPageTable.entrySet().size());
		UserKernel.pageLock.release();
	}
	
	/**
	 * get specified virtual page from global page table,
	 * assign new page when page fault,
	 * using random strategy
	 */
	public TranslationEntry getEntry(int pid, int vpn) {
		boolean needReleaseLock = false;
		if (vpn < 0 || vpn > maxVirtualPage.get(pid))
			UserKernel.currentProcess().handleException(nachos.machine.Processor.exceptionPageFault);
		
		if (!gptlock.isHeldByCurrentThread())
		{
			gptlock.acquire();
			needReleaseLock = true;
		}
		try
		{
			TranslationEntry entry = globalPageTable.get(new VPNInfo(pid, vpn));
			Integer pagenum;
			if (entry != null)
				return entry;
			if ((pagenum = UserKernel.availablePages.poll()) != null)
			{
				entry = new TranslationEntry(vpn, pagenum,
					true, false, false, false);
				
				Integer swapPos = pageInSwap.get(new VPNInfo(pid, vpn));
				if (swapPos==null)
				{
					SectionInfo sectionInfo = sectionMap.remove(new VPNInfo(pid, vpn));
					
					swapPos = assignSwapPos(pid, vpn);
					if (sectionInfo != null)
					{
						entry.readOnly = sectionInfo.section.isReadOnly();
						entry.dirty = true;
						sectionInfo.section.loadPage(sectionInfo.spn, pagenum);
					}
				}
				
				if (!entry.dirty)
					swap.read(swapPos * Processor.pageSize,
							nachos.machine.Machine.processor().getMemory(),
							pagenum * Processor.pageSize, Processor.pageSize);
				put(pid, vpn, entry);
			}
			else
			{
				pagenum = Lib.random(nachos.machine.Machine.processor().getNumPhysPages());
				entry = new TranslationEntry(vpn, pagenum,
						true, false, false, false);
				
				swapIn(new VPNInfo(pid, vpn), entry);
			}
			Lib.debug('x', "Page fault on pid: " + pid + 
					", vpn: " + vpn + ", ppn: " + entry.ppn);
			return entry;
		}
		catch (Throwable e)
		{
			System.err.println("Something wrong in getEntry()...");
			for(StackTraceElement a : e.getStackTrace())
				System.err.println(a.getFileName() + ": " + a.getLineNumber());
			System.err.flush();
			Lib.assertNotReached();
			return null;
		}
		finally{
			if (needReleaseLock)
				gptlock.release();
		}
	}
	
	public TranslationEntry[] getPageTable(int pid) {
		ArrayList<TranslationEntry> pageTable = new ArrayList<TranslationEntry>();
		for (Entry<VPNInfo, TranslationEntry> e : globalPageTable.entrySet())
			if (e.getKey().pid == pid)
				pageTable.add(e.getValue());
		java.util.Collections.sort(pageTable, new java.util.Comparator<TranslationEntry>(){

			@Override
			public int compare(TranslationEntry o1, TranslationEntry o2) {
				return o1.vpn - o2.vpn;
			}
			
		});
		return pageTable.toArray(new TranslationEntry[pageTable.size()]);
	}
	
	public void openSwap() {
		swap = nachos.threads.ThreadedKernel.fileSystem.open("SWAP", true);
	}
	
	public void closeSwap() {
		swap.close();
		nachos.threads.ThreadedKernel.fileSystem.remove("SWAP");
	}
	
	@Deprecated
	public void assign(int pid, int numPages) {
		gptlock.acquire();
		int vpn = 0;
		while (vpn < numPages)
			if (availablePosInSwap.isEmpty())
				pageInSwap.put(new VPNInfo(pid, vpn++), swapLen++);
			else
				pageInSwap.put(new VPNInfo(pid, vpn++), availablePosInSwap.removeFirst());	
		gptlock.release();
	}
	
	private int assignSwapPos(int pid, int vpn) {
		int pos;
		if (availablePosInSwap.isEmpty())
			pageInSwap.put(new VPNInfo(pid, vpn++), pos = (swapLen++));
		else
			pageInSwap.put(new VPNInfo(pid, vpn++), pos = availablePosInSwap.removeFirst());	
		return pos;
	}
	
	public void release(int pid) {
		gptlock.acquire();
		java.util.Iterator<java.util.Map.Entry<VPNInfo, Integer>> itr = pageInSwap.entrySet().iterator();
		while (itr.hasNext())
		{
			java.util.Map.Entry<VPNInfo, Integer> entry = itr.next();
			if (entry.getKey().pid == pid)
			{
				availablePosInSwap.add(entry.getValue());
				itr.remove();
			}
		}
		gptlock.release();
	}
	
	private void swapIn(VPNInfo vp, TranslationEntry entry){
		Lib.assertTrue(gptlock.isHeldByCurrentThread());
		int pp = entry.ppn;
		writeBack(vp.pid, pp);
		// unnecessary to set 'not dirty', because it would have done in writeBack before
		//Lib.assertTrue(globalPageTable.get(vp).dirty == false);
		Lib.debug('o', "swap in: " + vp.pid + ", " + vp.vpn + ", " + pp);
		Integer swapPos = pageInSwap.get(vp);
		if (swapPos==null)
		{
			SectionInfo sectionInfo = sectionMap.remove(vp);
			
			swapPos = assignSwapPos(vp.pid, vp.vpn);
			if (sectionInfo != null)
			{
				entry.readOnly = sectionInfo.section.isReadOnly();
				entry.dirty = true;
				sectionInfo.section.loadPage(sectionInfo.spn, pp);
			}
		}
		if (!entry.dirty)
			swap.read(swapPos * Processor.pageSize,
					nachos.machine.Machine.processor().getMemory(),
					pp * Processor.pageSize, Processor.pageSize);
		
		/*byte[] memory = nachos.machine.Machine.processor().getMemory();
		byte[] page = new byte[Processor.pageSize];
		DebugPage debugPage = null;
		System.arraycopy(memory, pp * Processor.pageSize, page, 0, Processor.pageSize);
		if ((debugPage = debugMap.get(vp)) != null){
			Lib.assertTrue(debugPage.equals(new DebugPage(page)));
		}*/
		
		globalPageTable.remove(PhysPagesMap.get(pp));
		put(vp, entry);
		Lib.debug('o', "Read physical page " + pp + " from " + swapPos * Processor.pageSize);
	}
	
	private void writeBack(int pid, int pp){
		for (int i = 0; i < nachos.machine.Machine.processor().getTLBSize(); ++i)
		{	
			TranslationEntry entry = nachos.machine.Machine.processor().readTLBEntry(i);
			if (entry.ppn == pp && entry.valid)
			{
				Lib.debug('o', "Update: pid" + UserKernel.currentProcess().pid + ", vpn" + entry.vpn + 
						", dirty: " + entry.dirty);
				entry.valid = false;
				updateEntry(UserKernel.currentProcess().pid, entry.vpn, entry.used, entry.dirty);
				nachos.machine.Machine.processor().writeTLBEntry(i, entry);
				break;
			}
		}
		
		VPNInfo oldvp = PhysPagesMap.get(pp);
		if (oldvp == null)
		{
			Lib.debug('o', "no old page");
			return;
		}
		if (globalPageTable.get(oldvp).dirty == false)
		{
			Lib.debug('o', "clean page, pid: " + oldvp.pid +
					", vpn: " + oldvp.vpn);
			return;
		}
		globalPageTable.get(oldvp).dirty = false;
		Integer swapPos = pageInSwap.get(oldvp);
		swap.write(swapPos * Processor.pageSize, 
				nachos.machine.Machine.processor().getMemory(), 
				pp * Processor.pageSize, Processor.pageSize);
		
		/*byte[] memory = nachos.machine.Machine.processor().getMemory();
		byte[] page = new byte[Processor.pageSize];
		System.arraycopy(memory, pp * Processor.pageSize, page, 0, Processor.pageSize);
		debugMap.put(oldvp, new DebugPage(page));*/
		
		Lib.debug('o', "Write physical page " + pp + " to " + swapPos * Processor.pageSize);
	}
	
	public void acquireLock(){
		gptlock.acquire();
	}
	
	public void releaseLock(){
		gptlock.release();
	}
	
	public static boolean lockNotHeldByOtherCurrent(){
		return gptlock.isNotHeldByOthers();
	}
	
	public boolean own(int pid, int vpn, int ppn) {
		VPNInfo info = PhysPagesMap.get(ppn);
		if (info == null)
			return false;
		return (info.pid == pid) && (info.vpn == vpn);
	}
	
	public void putSectionMap(int pid, int vpn, SectionInfo sectionInfo) {
		sectionMap.put(new VPNInfo(pid, vpn), sectionInfo);
	}
	
	public static java.util.Hashtable<VPNInfo, TranslationEntry> globalPageTable = 
			new java.util.Hashtable<VPNInfo, TranslationEntry>();
	public static java.util.HashMap<Integer, Integer> maxVirtualPage = 
			new java.util.HashMap<Integer, Integer>();
	private static java.util.Hashtable<Integer, VPNInfo> PhysPagesMap = 
			new java.util.Hashtable<Integer, VPNInfo>();
	private static java.util.LinkedList<Integer> availablePosInSwap = 
			new java.util.LinkedList<Integer>();
	private static java.util.HashMap<VPNInfo, Integer> pageInSwap = 
			new java.util.HashMap<VPNInfo, Integer>();
	public java.util.HashMap<VPNInfo, SectionInfo> sectionMap = new java.util.HashMap<VPNInfo, SectionInfo>();
	
	private static int swapLen = 0;
	private static OpenFile swap = null;
	private static nachos.threads.Lock gptlock = new nachos.threads.Lock();
	//private static java.util.HashMap<VPNInfo, DebugPage> debugMap = new java.util.HashMap<VPNInfo, DebugPage>();
	
}

class VPNInfo {
	VPNInfo(int _pid, int _vpn) {
		pid = _pid;
		vpn = _vpn;
	}
	
	protected int pid, vpn;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VPNInfo other = (VPNInfo) obj;
		if (pid != other.pid)
			return false;
		if (vpn != other.vpn)
			return false;
		return true;
	} 
	
	@Override
	public int hashCode()
	{
		return pid*1023 + vpn;
	}
}

/*class DebugPage{
	DebugPage(byte[] _page){
		page = _page;
	}
	
	@Override
	public boolean equals(Object obj){
		if (obj == null || obj.getClass()!=this.getClass())
			return false;
		DebugPage other = (DebugPage)obj;
		if (other.page.length != this.page.length)
			return false;
		for (int i=0; i<page.length; ++i)
			if (page[i]!=other.page[i])
				return false;
		return true;
	}
	
	protected byte[] page;
}*/