package nachos.filesys;

import nachos.machine.Lib;

public class SymLink extends GlobalFile 
{	
	@Deprecated
	private SymLink(INode inode, GlobalFile dst)
	{
		super(inode);
		inode.file_type = INode.TYPE_SYMLINK;
		this.dst = dst;
		if (dst != null)
		{
			byte[] dstinode = Lib.bytesFromInt(dst.inode.inodeid);
			super.write(0, dstinode, 0, 2);
		}
	}
	
	public void load()
	{
		if (super.length() == 0)
			return;
		byte[] buffer = new byte[2];
		super.read(0, buffer, 0, 2);
		int inodeid = Lib.bytesToInt(buffer, 0);
		this.dst = GlobalFile.getNewGlobalFile(INode.getNthINode(inodeid));
	}
	
	static public SymLink createNewSymlink(INode inode, GlobalFile dst)
	{
		if (dst != null && dst.inode.file_type == INode.TYPE_SYMLINK)
			return null;
		GlobalFile file = FilesysKernel.realFileSystem.globalFiles.get(inode.inodeid);
		if (file != null)
			return null;
		SymLink link = new SymLink(inode, dst);
		FilesysKernel.realFileSystem.
			globalFiles.put(inode.inodeid, link);
		return link;
	}
	
	static public SymLink getSymlink(INode inode)
	{
		GlobalFile file = FilesysKernel.realFileSystem.globalFiles.get(inode.inodeid);
		if (file == null || inode.file_type != INode.TYPE_SYMLINK)
			return null;
		return (SymLink)file;
	}
	
	protected GlobalFile dst;
}
