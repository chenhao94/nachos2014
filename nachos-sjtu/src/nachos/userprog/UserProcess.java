package nachos.userprog;

import nachos.machine.*;
import nachos.threads.*;
import nachos.vm.SectionInfo;

import java.io.EOFException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Encapsulates the state of a user process that is not contained in its user
 * thread (or threads). This includes its address translation state, a file
 * table, and information about the program being executed.
 * 
 * <p>
 * This class is extended by other classes to support additional functionality
 * (such as additional syscalls).
 * 
 * @see nachos.vm.VMProcess
 * @see nachos.network.NetProcess
 */
public class UserProcess {
	/**
	 * Allocate a new process.
	 */
	public UserProcess() {
		pid = processInfo.processCntInc();
	}

	/**
	 * Allocate and return a new process of the correct class. The class name is
	 * specified by the <tt>nachos.conf</tt> key
	 * <tt>Kernel.processClassName</tt>.
	 * 
	 * @return a new process of the correct class.
	 */
	public static UserProcess newUserProcess() {
		return (UserProcess) Lib.constructObject(Machine.getProcessClassName());
	}

	/**
	 * Execute the specified program with the specified arguments. Attempts to
	 * load the program, and then forks a thread to run it.
	 * 
	 * @param name
	 *            the name of the file containing the executable.
	 * @param args
	 *            the arguments to pass to the executable.
	 * @return <tt>true</tt> if the program was successfully executed.
	 */
	public boolean execute(String name, String[] args) {
		Arrays.fill(fileDescriptors, null);
		fileDescriptors[0] = UserKernel.console.openForReading();
		fileDescriptors[1] = UserKernel.console.openForWriting();
		toBeUnlinked.clear();
		Lib.debug('x', "Exec: pid: " + pid);
		
		if (!load(name, args))
			return false;
		
		Lib.debug('x', "Exec success, pid: " + pid);
		runningStatus = 1;
		processInfo.putRunning(pid, this);
		thread = new UThread(this);
		thread.setName(name).fork();

		return true;
	}

	/**
	 * Save the state of this process in preparation for a context switch.
	 * Called by <tt>UThread.saveState()</tt>.
	 */
	public void saveState() {
	}

	/**
	 * Restore the state of this process after a context switch. Called by
	 * <tt>UThread.restoreState()</tt>.
	 */
	public void restoreState() {
		//Machine.processor().setPageTable(pageTable);
		Machine.processor().setPageTable(globalPageTable.getPageTable(pid));
	}
	
	public int readVirtualMemoryInteger(int vaddr) throws Exception
	{
		if (vaddr % 4 != 0)
			throw new RuntimeException("Bad aligned address.");
		byte[] tmp = new byte[4];
		if (readVirtualMemory(vaddr, tmp) != 4)
			throw new RuntimeException("Error in reading");
		return Lib.bytesToInt(tmp, 0);
	}
	
	public int writeVirtualMemoryInteger(int vaddr, int value) throws Exception
	{
		if (vaddr % 4 != 0)
			throw new RuntimeException("Bad aligned address.");
		byte[] tmp = Lib.bytesFromInt(value);
		if (writeVirtualMemory(vaddr, tmp) != 4)
			throw new RuntimeException("Error in writing");
		return 0;
	}
	
	public String readVirtualMemoryString(int vaddr)
	{
		return readVirtualMemoryString(vaddr, nachos.filesys.FileStat.FILE_NAME_MAX_LEN);
	}

	/**
	 * Read a null-terminated string from this process's virtual memory. Read at
	 * most <tt>maxLength + 1</tt> bytes from the specified address, search for
	 * the null terminator, and convert it to a <tt>java.lang.String</tt>,
	 * without including the null terminator. If no null terminator is found,
	 * returns <tt>null</tt>.
	 * 
	 * @param vaddr
	 *            the starting virtual address of the null-terminated string.
	 * @param maxLength
	 *            the maximum number of characters in the string, not including
	 *            the null terminator.
	 * @return the string read, or <tt>null</tt> if no null terminator was
	 *         found.
	 */
	public String readVirtualMemoryString(int vaddr, int maxLength) {
		Lib.assertTrue(maxLength >= 0);

		byte[] bytes = new byte[maxLength + 1];

		int bytesRead = readVirtualMemory(vaddr, bytes);

		for (int length = 0; length < bytesRead; length++) {
			if (bytes[length] == 0)
				return new String(bytes, 0, length);
		}

		return null;
	}

	/**
	 * Transfer data from this process's virtual memory to all of the specified
	 * array. Same as <tt>readVirtualMemory(vaddr, data, 0, data.length)</tt>.
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to read.
	 * @param data
	 *            the array where the data will be stored.
	 * @return the number of bytes successfully transferred.
	 */
	public int readVirtualMemory(int vaddr, byte[] data) {
		return readVirtualMemory(vaddr, data, 0, data.length);
	}

	/**
	 * Transfer data from this process's virtual memory to the specified array.
	 * This method handles address translation details. This method must
	 * <i>not</i> destroy the current process if an error occurs, but instead
	 * should return the number of bytes successfully copied (or zero if no data
	 * could be copied).
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to read.
	 * @param data
	 *            the array where the data will be stored.
	 * @param offset
	 *            the first byte to write in the array.
	 * @param length
	 *            the number of bytes to transfer from virtual memory to the
	 *            array.
	 * @return the number of bytes successfully transferred.
	 */
	public int readVirtualMemory(int vaddr, byte[] data, int offset, int length) {
		Lib.assertTrue(offset >= 0 && length >= 0
				&& offset + length <= data.length);

		byte[] memory = Machine.processor().getMemory();
		int vpn = getVPN(vaddr), bytesLeft = distToPageEnd(vaddr);
		int ppn, amount, sum = 0;
		
		if (vpn < 0 || vpn >= numPages)
			return sum;
		//ppn = pageTable[vpn].ppn;
		ppn = globalPageTable.getPPN(pid, vpn, false);
		amount = Math.min(bytesLeft, length);
		System.arraycopy(memory, ppn*pageSize + vaddr%pageSize, data, offset, amount);
		
		offset += amount;
		length -= amount;
		sum += amount;
		++vpn;
		
		while (length > 0)
		{
			if (vpn >= numPages)
				return sum;
			//ppn = pageTable[vpn].ppn;
			ppn = globalPageTable.getPPN(pid, vpn, false);
			amount = Math.min(pageSize, length);
			System.arraycopy(memory, ppn*pageSize, data, offset, amount);
			
			offset += amount;
			length -= amount;
			sum += amount;
			++vpn;
		}
		
		return sum;
	}

	/**
	 * Transfer all data from the specified array to this process's virtual
	 * memory. Same as <tt>writeVirtualMemory(vaddr, data, 0, data.length)</tt>.
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to write.
	 * @param data
	 *            the array containing the data to transfer.
	 * @return the number of bytes successfully transferred.
	 */
	public int writeVirtualMemory(int vaddr, byte[] data) {
		return writeVirtualMemory(vaddr, data, 0, data.length);
	}

	/**
	 * Transfer data from the specified array to this process's virtual memory.
	 * This method handles address translation details. This method must
	 * <i>not</i> destroy the current process if an error occurs, but instead
	 * should return the number of bytes successfully copied (or zero if no data
	 * could be copied).
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to write.
	 * @param data
	 *            the array containing the data to transfer.
	 * @param offset
	 *            the first byte to transfer from the array.
	 * @param length
	 *            the number of bytes to transfer from the array to virtual
	 *            memory.
	 * @return the number of bytes successfully transferred.
	 */
	public int writeVirtualMemory(int vaddr, byte[] data, int offset, int length) {
		Lib.assertTrue(offset >= 0 && length >= 0
				&& offset + length <= data.length);

		byte[] memory = Machine.processor().getMemory();

		int vpn = getVPN(vaddr), bytesLeft = distToPageEnd(vaddr);
		int ppn, amount, sum = 0;
		
		if (vpn < 0 || vpn >= numPages)
			return sum;
		//ppn = pageTable[vpn].ppn;
		ppn = globalPageTable.getPPN(pid, vpn, true);
		amount = Math.min(bytesLeft, length);
		System.arraycopy(data, offset, memory, ppn*pageSize + vaddr%pageSize, amount);
		
		offset += amount;
		length -= amount;
		sum += amount;
		++vpn;
		
		while (length > 0)
		{
			if (vpn >= numPages)
				return sum;
			//ppn = pageTable[vpn].ppn;
			ppn = globalPageTable.getPPN(pid, vpn, true);
			amount = Math.min(pageSize, length);
			System.arraycopy(data, offset, memory, ppn*pageSize, amount);
			
			offset += amount;
			length -= amount;
			sum += amount;
			++vpn;
		}
		
		return sum;
	}

	/**
	 * Load the executable with the specified name into this process, and
	 * prepare to pass it the specified arguments. Opens the executable, reads
	 * its header information, and copies sections and arguments into this
	 * process's virtual memory.
	 * 
	 * @param name
	 *            the name of the file containing the executable.
	 * @param args
	 *            the arguments to pass to the executable.
	 * @return <tt>true</tt> if the executable was successfully loaded.
	 */
	private boolean load(String name, String[] args) {
		Lib.debug(dbgProcess, "UserProcess.load(\"" + name + "\")");

		OpenFile executable = ThreadedKernel.fileSystem.open(name, false);
		if (executable == null) {
			Lib.debug(dbgProcess, "\topen failed");
			return false;
		}

		try {
			coff = new Coff(executable);
		} catch (EOFException e) {
			executable.close();
			Lib.debug(dbgProcess, "\tcoff load failed");
			return false;
		}

		// make sure the sections are contiguous and start at page 0
		numPages = 0;
		for (int s = 0; s < coff.getNumSections(); s++) {
			CoffSection section = coff.getSection(s);
			if (section.getFirstVPN() != numPages) {
				coff.close();
				Lib.debug(dbgProcess, "\tfragmented executable");
				return false;
			}
			numPages += section.getLength();
		}

		// make sure the argv array will fit in one page
		byte[][] argv = new byte[args.length][];
		int argsSize = 0;
		for (int i = 0; i < args.length; i++) {
			argv[i] = args[i].getBytes();
			// 4 bytes for argv[] pointer; then string plus one for null byte
			argsSize += 4 + argv[i].length + 1;
		}
		if (argsSize > pageSize) {
			coff.close();
			Lib.debug(dbgProcess, "\targuments too long");
			return false;
		}

		// program counter initially points at the program entry point
		initialPC = coff.getEntryPoint();

		// next comes the stack; stack pointer initially points to top of it
		numPages += stackPages;
		initialSP = numPages * pageSize;

		// and finally reserve 1 page for arguments
		numPages++;
		GlobalPageTable.maxVirtualPage.put(pid, numPages);
		
		if (!loadSections())
			return false;

		// store arguments in last page
		int entryOffset = (numPages - 1) * pageSize;
		int stringOffset = entryOffset + args.length * 4;

		this.argc = args.length;
		this.argv = entryOffset;

		for (int i = 0; i < argv.length; i++) {
			byte[] stringOffsetBytes = Lib.bytesFromInt(stringOffset);
			Lib
					.assertTrue(writeVirtualMemory(entryOffset,
							stringOffsetBytes) == 4);
			entryOffset += 4;
			Lib
					.assertTrue(writeVirtualMemory(stringOffset, argv[i]) == argv[i].length);
			stringOffset += argv[i].length;
			Lib
					.assertTrue(writeVirtualMemory(stringOffset,
							new byte[] { 0 }) == 1);
			stringOffset += 1;
		}

		return true;
	}

	/**
	 * Allocates memory for this process, and loads the COFF sections into
	 * memory. If this returns successfully, the process will definitely be run
	 * (this is the last step in process initialization that can fail).
	 * 
	 * @return <tt>true</tt> if the sections were successfully loaded.
	 */
	protected boolean loadSections() {
		//pageTable = new TranslationEntry[numPages];
		allocatePages(numPages);
		
		if (numPages > Machine.processor().getNumPhysPages()) {
			coff.close();
			Lib.debug(dbgProcess, "\tinsufficient physical memory");
			return false;
		}

		// load sections
		for (int s = 0; s < coff.getNumSections(); s++) {
			CoffSection section = coff.getSection(s);

			Lib.debug(dbgProcess, "\tinitializing " + section.getName()
					+ " section (" + section.getLength() + " pages)");

			for (int i = 0; i < section.getLength(); i++) {
				int vpn = section.getFirstVPN() + i;
				//pageTable[vpn].readOnly = section.isReadOnly();
				globalPageTable.setReadOnly(pid, vpn, section.isReadOnly());
				//section.loadPage(i, pageTable[vpn].ppn);
				section.loadPage(i, globalPageTable.getPPN(pid, vpn, true));
				Lib.debug('x', "Load pid: " + pid + ", vpn: " + vpn);
			}
		}
		coff.close();
		return true;
	}

	/**
	 * Release any resources allocated by <tt>loadSections()</tt>.
	 */
	protected void unloadSections() {
	}

	/**
	 * Initialize the processor's registers in preparation for running the
	 * program loaded into this process. Set the PC register to point at the
	 * start function, set the stack pointer register to point at the top of the
	 * stack, set the A0 and A1 registers to argc and argv, respectively, and
	 * initialize all other registers to 0.
	 */
	public void initRegisters() {
		Processor processor = Machine.processor();

		// by default, everything's 0
		for (int i = 0; i < Processor.numUserRegisters; i++)
			processor.writeRegister(i, 0);

		// initialize PC and SP according
		processor.writeRegister(Processor.regPC, initialPC);
		processor.writeRegister(Processor.regSP, initialSP);

		// initialize the first two argument registers to argc and argv
		processor.writeRegister(Processor.regA0, argc);
		processor.writeRegister(Processor.regA1, argv);
	}

	/**
	 * Handle the halt() system call.
	 */
	private int handleHalt() {
		Lib.debug('x', "handleHalt");
		if (pid != 1)
			return 0;
		
		closeAll();
		releasePages();
		
		Machine.halt();

		Lib.assertNotReached("Machine.halt() did not halt machine!");
		return 0;
	}

	private static final int syscallHalt = 0, syscallExit = 1, syscallExec = 2,
			syscallJoin = 3, syscallCreate = 4, syscallOpen = 5,
			syscallRead = 6, syscallWrite = 7, syscallClose = 8,
			syscallUnlink = 9;

	/**
	 * Handle a syscall exception. Called by <tt>handleException()</tt>. The
	 * <i>syscall</i> argument identifies which syscall the user executed:
	 * 
	 * <table>
	 * <tr>
	 * <td>syscall#</td>
	 * <td>syscall prototype</td>
	 * </tr>
	 * <tr>
	 * <td>0</td>
	 * <td><tt>void halt();</tt></td>
	 * </tr>
	 * <tr>
	 * <td>1</td>
	 * <td><tt>void exit(int status);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>2</td>
	 * <td><tt>int  exec(char *name, int argc, char **argv);
     * 								</tt></td>
	 * </tr>
	 * <tr>
	 * <td>3</td>
	 * <td><tt>int  join(int pid, int *status);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>4</td>
	 * <td><tt>int  creat(char *name);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>5</td>
	 * <td><tt>int  open(char *name);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>6</td>
	 * <td><tt>int  read(int fd, char *buffer, int size);
     *								</tt></td>
	 * </tr>
	 * <tr>
	 * <td>7</td>
	 * <td><tt>int  write(int fd, char *buffer, int size);
     *								</tt></td>
	 * </tr>
	 * <tr>
	 * <td>8</td>
	 * <td><tt>int  close(int fd);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>9</td>
	 * <td><tt>int  unlink(char *name);</tt></td>
	 * </tr>
	 * </table>
	 * 
	 * @param syscall
	 *            the syscall number.
	 * @param a0
	 *            the first syscall argument.
	 * @param a1
	 *            the second syscall argument.
	 * @param a2
	 *            the third syscall argument.
	 * @param a3
	 *            the fourth syscall argument.
	 * @return the value to be returned to the user.
	 */
	public int handleSyscall(int syscall, int a0, int a1, int a2, int a3) {
		switch (syscall) {
		case syscallHalt:
			return handleHalt();
		case syscallExit:
			return handleExit(a0);
		case syscallExec:
			try{
				return handleExec(readVirtualMemoryString(a0) ,a1 ,getArgv(a1, a2));
			}
			catch (Throwable e)
			{
				return -1;
			}
		case syscallJoin:
			return handleJoin(a0, a1);
		case syscallCreate:
			return handleCreate(readVirtualMemoryString(a0));
		case syscallOpen:
			return handleOpen(readVirtualMemoryString(a0));
		case syscallRead:
			return handleRead(a0, a1, a2);
		case syscallWrite:
			return handleWrite(a0, a1, a2);
		case syscallClose:
			return handleClose(a0);
		case syscallUnlink:
			return handleUnlink(readVirtualMemoryString(a0));

		default:
			Lib.debug(dbgProcess, "Unknown syscall " + syscall);
			processInfo.getRunning(pid).runningStatus = 0;
			handleExit(-1);
		}
		return -1;
	}

	private int handleJoin(int cpid, int vaddr) {
		Lib.debug('h', "handleJoin");
		UserProcess child;
		if ((child = processInfo.getRunning(cpid))==null || child.father != pid)
			return -1;
		child.father = 0;
		child.thread.join();
		processInfo.removeRunning(cpid);
		int status = processInfo.removeTerminated(cpid);//terminated.remove(cpid);
		try {
			this.writeVirtualMemoryInteger(vaddr, status);
		}
		catch (Throwable e)
		{
			System.err.println(e.getMessage());
			return -1;
		}
		return child.runningStatus;
	}

	private int handleExec(String filename, int argc,
			ArrayList<String> argv) {
		int cpid = processInfo.getProcessCnt() + 1;
		Lib.debug('x', "handleExec: pid: " + cpid);
		UserProcess child = UserProcess.newUserProcess();
		child.father = pid;
		child.runningStatus = 1;
		Lib.debug('h', "handleExec check 1");
		//processInfo.putRunning(cpid, child);
		if (!child.execute(filename, argv.toArray(new String[0])))
		{
			//processInfo.removeRunning(cpid);
			Lib.debug('h', "handleExec failed.");
			return -1;
		}
		Lib.debug('h', "handleExec sucess, pid: " + cpid);
		return cpid;
	}

	private int handleExit(int status) {
		Lib.debug('x', "handleExit: pid: " + pid);
		closeAll();
		releasePages();
		processInfo.putTerminated(pid, status);
		if (processInfo.getRunningSize() == processInfo.getTerminatedSize())
			nachos.machine.Machine.halt();
		UThread.finish();
		Lib.assertNotReached("not correctly exit!");
		return 0;
	}

	private int handleCreate(String filename) {
		Lib.debug('h', "handleCreate");
		if (toBeUnlinked.containsKey(filename))
			return -1;
		OpenFile	file = nachos.threads.ThreadedKernel.fileSystem.open(filename, true);
		return getFileDescriptor(file);
	}

	private int handleOpen(String filename) {
		Lib.debug('h', "handleOpen");
		if (toBeUnlinked.containsKey(filename))
			return -1;
		OpenFile file = nachos.threads.ThreadedKernel.fileSystem.open(filename, false);
		return getFileDescriptor(file);
	}
	
	private int getFileDescriptor(OpenFile file)
	{
		if (file == null)
			return -1;
		int index;
		for (index = 0; index < 16; ++index)
			if (fileDescriptors[index] == null)
			{
				fileDescriptors[index] = file;
				break;
			}
		if (index >= 16)
		{
			file.close();
			return -1;
		}
		return index;
	}

	private int handleRead(int fd, int buf_addr, int count) {
		Lib.debug('h', "handleRead");
		if (fd > 15 || fd < 0)
			return -1;
		OpenFile file = fileDescriptors[fd];
		if (file == null)
			return -1;
		byte[] buf = new byte[count];
		int ret = file.read(buf, 0, count);
		if (ret < 0 || ret != this.writeVirtualMemory(buf_addr, buf, 0, ret))
			return -1;
		return ret;
	}
	
	private int handleWrite(int fd, int buf_addr, int count) {
		Lib.debug('h', "handleWrite");
		if (fd > 15 || fd < 0)
		{	
			Lib.debug('h', "handleWrite failed on pos1.");
			return -1;
		}
		OpenFile file = fileDescriptors[fd];
		if (file == null)
		{	
			Lib.debug('h', "handleWrite failed on pos2.");
			return -1;
		}
		byte[] buf = new byte[count];
		int ret = this.readVirtualMemory(buf_addr, buf);
		if (ret < 0 || ret != file.write(buf, 0, ret))
		{	
			Lib.debug('h', "handleWrite failed on pos3.");
			return -1;
		}
		return ret;
	}
	
	private int handleClose(int fd) {
		Lib.debug('h', "handleClose");
		if (fd > 15 || fd < 0)
			return -1;
		OpenFile file = fileDescriptors[fd];
		if (file == null)
			return -1;
		String filename = file.getName();
		file.close();
		fileDescriptors[fd] = null;
		if (toBeUnlinked.containsKey(filename))
		{
			int v = toBeUnlinked.get(filename);
			if (v > 1)
				toBeUnlinked.put(filename, v-1);
			else
			{
				toBeUnlinked.remove(filename);
				nachos.threads.ThreadedKernel.fileSystem.remove(filename);
			}
		}
		return 0;
	}

	private int handleUnlink(String filename) {
		Lib.debug('h', "handleUnlink");
		boolean using = false;
		for (OpenFile fd : fileDescriptors)
			if (fd != null && fd.getName().equals(filename))
			{
				using = true;
				if (toBeUnlinked.containsKey(filename))
					toBeUnlinked.put(filename, toBeUnlinked.get(filename)+1);
				else
					toBeUnlinked.put(filename, 1);
			}
		if (!using)
		{
			boolean succ = nachos.threads.ThreadedKernel.fileSystem.remove(filename);
			if (succ)
				return 0;
			return -1;
		}
		return 0;
	}

	/**
	 * Handle a user exception. Called by <tt>UserKernel.exceptionHandler()</tt>
	 * . The <i>cause</i> argument identifies which exception occurred; see the
	 * <tt>Processor.exceptionZZZ</tt> constants.
	 * 
	 * @param cause
	 *            the user exception that occurred.
	 */
	public void handleException(int cause) {
		//Lib.debug('h', "handleException: " + Processor.exceptionNames[cause]);
		Processor processor = Machine.processor();
		
		switch (cause) {
		case Processor.exceptionSyscall:
			int result = handleSyscall(processor.readRegister(Processor.regV0),
					processor.readRegister(Processor.regA0), processor
							.readRegister(Processor.regA1), processor
							.readRegister(Processor.regA2), processor
							.readRegister(Processor.regA3));
			processor.writeRegister(Processor.regV0, result);
			processor.advancePC();
			
			break;

		default:
			Lib.debug('x', "handleException");
			Lib.debug(dbgProcess, "Unexpected exception: "
					+ Processor.exceptionNames[cause]);
			processInfo.getRunning(pid).runningStatus = 0;
			handleExit(-1);
			//Lib.assertNotReached("Unexpected exception");
		}
	}
	
	protected boolean allocatePages(int num)
	{
		Integer pagenum;
		Lib.debug('x', "need pages num: " + numPages);
		UserKernel.pageLock.acquire();
		for (int i = 0; i < num; i++)
		{	
			pagenum = UserKernel.availablePages.poll();
			if (pagenum == null)
			{
				UserKernel.pageLock.release();
				return false;
			}
			//pageTable[i] = new TranslationEntry(i, pagenum, true, false, false, false);
			globalPageTable.put(pid, i, new TranslationEntry(i, pagenum, true, false, false, false));
		}
		UserKernel.pageLock.release();
		return true;
	}
	
	protected void releasePages() {
		globalPageTable.releasePages(pid);
	}
	
	private void closeAll()
	{
		for (int fd = 0; fd < 16; ++fd)
			if (fileDescriptors[fd] != null)
				handleClose(fd);
		this.unloadSections();
		Lib.debug('x', "Files opening: " + ((StubFileSystem)(ThreadedKernel.fileSystem)).getOpenCount());
	}
	
	/** return the virtual page number */
	public static int getVPN(int vaddr)
	{
		return vaddr / pageSize;
	}
	
	/** The number of bytes from vaddr to the end of the page*/
	protected int distToPageEnd(int vaddr)
	{
		return pageSize - vaddr % pageSize;
	}

	private java.util.ArrayList<String> getArgv(int argc, int argvAddr) throws Exception {
		java.util.ArrayList<String> argv = new java.util.ArrayList<String>();
		int addr;
		String arg;
		while (argc > 0)
		{
			addr = readVirtualMemoryInteger(argvAddr);
			arg = readVirtualMemoryString(addr);
			if (arg == null)
				throw new RuntimeException("Bad address in argv");
			argv.add(arg);
			argvAddr += 4;
			--argc;
		}
		return argv;
	}

	public int getPid() {
		return pid;
	}
	
	/** The program being run by this process. */
	protected Coff coff;

	/** This process's page table. */
	//protected TranslationEntry[] pageTable;
	/** The number of contiguous pages occupied by the program. */
	protected int numPages;

	/** The number of pages in the program's stack. */
	protected final int stackPages = Config.getInteger("Processor.numStackPages", 8);

	private int initialPC, initialSP;
	private int argc, argv;
	private OpenFile[] fileDescriptors = new OpenFile[16];
	private java.util.HashMap<String, Integer> toBeUnlinked = new HashMap<String, Integer>();
	private int father = 0;
	protected int pid, runningStatus;
	private UThread thread;
	
	private static final int pageSize = Processor.pageSize;
	private static final char dbgProcess = 'a';
	private static ProcessInfo processInfo = new ProcessInfo();
	public/*protected*/ static GlobalPageTable globalPageTable = new GlobalPageTable();
	
	private static class ProcessInfo{
		public ProcessInfo() {}
		
		public UserProcess getRunning(int pid) {
			return running.get(pid);
		}
		
		public void putTerminated(int pid, int status)
		{
			lock.acquire();
			terminated.put(pid, status);
			lock.release();
		}
		
		public void putRunning(int pid, UserProcess process) {
			lock.acquire();
			running.put(pid, process);
			lock.release();
		}
		
		public UserProcess removeRunning(int pid) {
			lock.acquire();
			UserProcess ret = running.remove(pid);
			lock.release();
			return ret;
		}
		
		public Integer removeTerminated(int pid) {
			lock.acquire();
			Integer ret = terminated.remove(pid);
			lock.release();
			return ret;
		}
		
		public int getRunningSize() {
			return running.size();
		}
		
		public int getTerminatedSize() {
			return terminated.size();
		}
		
		public int getProcessCnt() {
			return this.processCnt;
		}
		
		public int processCntInc() {
			lock.acquire();
			int ret = ++this.processCnt;
			lock.release();
			return ret;
		}
		
		private java.util.HashMap<Integer, UserProcess> running = 
				new java.util.HashMap<Integer, UserProcess>();
		private java.util.HashMap<Integer, Integer> terminated = 
				new java.util.HashMap<Integer, Integer>();
		private int processCnt = 0;
		private Lock lock = new Lock();
	}

}
