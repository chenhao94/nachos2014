package nachos.vm;

import nachos.machine.CoffSection;

public class SectionInfo {
	public SectionInfo(int sid, int spn, CoffSection section) {
		this.sid = sid;
		this.spn = spn;
		this.section = section;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + sid;
		result = prime * result + spn;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SectionInfo other = (SectionInfo) obj;
		if (sid != other.sid)
			return false;
		if (spn != other.spn)
			return false;
		return true;
	}

	public int sid, spn;
	public CoffSection section;
}
