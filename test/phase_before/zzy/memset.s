	.file	1 "memset.c"
	.text
	.align	2
	.globl	memset
	.ent	memset
memset:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, extra= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	beq	$6,$0,$L8
	move	$7,$0
	.set	macro
	.set	reorder

$L6:
	addu	$3,$4,$7
	addu	$7,$7,1
	sltu	$2,$7,$6
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L6
	sb	$5,0($3)
	.set	macro
	.set	reorder

$L8:
	.set	noreorder
	.set	nomacro
	j	$31
	move	$2,$4
	.set	macro
	.set	reorder

	.end	memset
