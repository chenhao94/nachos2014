package nachos.filesys;

import nachos.machine.Disk;
import nachos.machine.Lib;

/**
 * FreeList is a static part (located in sector 0) of disk used to manage free space of the filesystem.
 * It maintains a list of sector numbers to indicate those that are available to use.
 * When there's a need to allocate a new sector in the filesystem, call allocate().
 * And you should call deallocate() to free space at a appropriate time (eg. when a file is deleted) for reuse in the future.
 * 
 */
public class FreeList
{
  /** the static address */
  public static int STATIC_ADDR = 0;
  
  /** size occupied in the disk (bitmap) */
  static int size = Lib.divRoundUp(Disk.NumSectors, 8);
  
  /** maintain address of all the free sectors */
  private java.util.BitSet free_list;
  
  public FreeList ()
  {
	  free_list = new java.util.BitSet(Disk.NumSectors);
  }
  
  public void init (boolean format)
  {
	  if (format)
	  {
		  Lib.debug('f', "free list init: " + Disk.NumSectors);
		  free_list.set(1, Disk.NumSectors);
	  }
	  else
		  load();
  }
  
  public int sectorsRemain()
  {
	 return free_list.cardinality(); 
  }
  
  /** allocate a new sector in the disk 
   * 	@return if success, return the sector number, o.w., -1
   */
  public int allocate ()
  {
	int sectno = free_list.nextSetBit(0);
	if (sectno >= 0)
		free_list.clear(sectno);
    return sectno;
  }
  
  /** deallocate a sector to be reused */
  public void deallocate (int sec)
  {
    free_list.set(sec);
  }
  
  /** save the content of freelist to the disk */
  public void save ()
  {
	  byte[] sector = new byte[Disk.SectorSize];
	  for (int i = 0; i < Disk.SectorSize; ++i)
	  {
		  byte b = 0;
		  for (int j = 0; j < 8; ++j)
			  if (free_list.get(8 * i + j))
				  b |= (1<<j);
		  sector[i] = b;
	  }
	  nachos.machine.Machine.synchDisk().writeSector(STATIC_ADDR, sector, 0);
  }
  
  /** load the content of freelist from the disk */
  public void load ()
  {
	  byte[] sector = new byte[Disk.SectorSize];
	  nachos.machine.Machine.synchDisk().readSector(STATIC_ADDR, sector, 0);
	  for (int i = 0; i < Disk.SectorSize; ++i)
	  {
		  byte b = sector[i];
		  for (int j = 0; j < 8; ++j)
			  if ((b & (1 << j)) != 0)
				  free_list.set(8 * i + j);
	  }
  }

	public boolean contains(int sectno)
	{
		return free_list.get(sectno);
	}
}
