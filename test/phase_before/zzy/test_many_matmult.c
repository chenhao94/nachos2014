#include "syscall.h"
#include "stdlib.h"

#define ASSERT_TRUE( expr, ...)	\
	do {	\
		if (!(expr)) {	\
			printf("assertion failure: %s(%d): ", __FILE__, __LINE__);	\
			printf(__VA_ARGS__);	\
			exit(1);	\
		}	\
	} while(0)

int main(int argc , char *argv[] ) {
	char* argv2[] = {"matmult.coff"};
	int i;
	for (i = 0; i < 1000; ++i) {
		int pid;
		ASSERT_TRUE( (pid = exec(argv2[0], 1, argv2)) > 0);
		ASSERT_TRUE( join(pid, 0) == 1 );
	}

    return 0;
}   
