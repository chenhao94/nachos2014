#include "syscall.h"
#include "stdio.h"

int main(int argc, char* argv[]) {
	if (argc == 1) {
		char* argv[] = { "test_last_exit.coff", "1" };
		exec(argv[0], 2, argv);
		printf("I'm root, and I'm done\n");
		exit(0);
	}

	else {
		int i;
		for (i = 0; i < 1000000; ++i);

		printf("I'm forked, and I'm done\n");
		exit(0);
	}

	return 0;
}
