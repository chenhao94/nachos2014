package nachos.threads;

import nachos.machine.Lib;
import nachos.machine.Machine;

/**
 * A scheduler that chooses threads using a lottery.
 * 
 * <p>
 * A lottery scheduler associates a number of tickets with each thread. When a
 * thread needs to be dequeued, a random lottery is held, among all the tickets
 * of all the threads waiting to be dequeued. The thread that holds the winning
 * ticket is chosen.
 * 
 * <p>
 * Note that a lottery scheduler must be able to handle a lot of tickets
 * (sometimes billions), so it is not acceptable to maintain state for every
 * ticket.
 * 
 * <p>
 * A lottery scheduler must partially solve the priority inversion problem; in
 * particular, tickets must be transferred through locks, and through joins.
 * Unlike a priority scheduler, these tickets add (as opposed to just taking the
 * maximum).
 */
public class LotteryScheduler extends PriorityScheduler {
	/**
	 * Allocate a new lottery scheduler.
	 */
	public LotteryScheduler() {
	}

	/**
	 * Allocate a new lottery thread queue.
	 * 
	 * @param transferPriority
	 *            <tt>true</tt> if this queue should transfer tickets from
	 *            waiting threads to the owning thread.
	 * @return a new lottery thread queue.
	 */
	public ThreadQueue newThreadQueue(boolean transferPriority) {
		return new LotteryPriorityQueue(transferPriority);
	}
	
	public void setPriority(KThread thread, int priority) {
		Lib.assertTrue(Machine.interrupt().disabled());

		Lib.assertTrue(priority >= priorityMinimum
				&& priority <= priorityMaximum);
		
		getThreadState(thread).setPriority(priority);
	}

	public boolean increasePriority()
	{
		boolean intStatus = Machine.interrupt().disable();

		KThread thread = KThread.currentThread();

		int priority = getPriority(thread);
		if (priority == priorityMaximum)
		{
		  Machine.interrupt().restore(intStatus); // bug identified by Xiao Jia @ 2011-11-04
			return false;
		}

		setPriority(thread, priority + 1);

		Machine.interrupt().restore(intStatus);
		return true;
	}
	
	public boolean decreasePriority()
	{
		boolean intStatus = Machine.interrupt().disable();

		KThread thread = KThread.currentThread();

		int priority = getPriority(thread);
		if (priority == priorityMinimum)
		{
		  Machine.interrupt().restore(intStatus); // bug identified by Xiao Jia @ 2011-11-04
			return false;
		}

		setPriority(thread, priority - 1);

		Machine.interrupt().restore(intStatus);
		return true;
	}

	protected ThreadState getThreadState(KThread thread) {
		Lib.assertTrue(thread != null);
		if (thread.schedulingState == null)
			thread.schedulingState = new LotteryThreadState(thread);

		return (ThreadState) thread.schedulingState;
	}
	
	protected class LotteryPriorityQueue extends PriorityQueue {

		LotteryPriorityQueue(boolean transferPriority) {
			super(transferPriority);
			// TODO Auto-generated constructor stub
		}
		
		protected ThreadState pickNextThread() {
			Lib.debug('y', "Use subclass member.");
			if (waitList.isEmpty())
				return null;
			int lottery = Lib.random(getTicketSum());
			for (KThread thread : waitList)
			{
				lottery -= getThreadState(thread).getEffectivePriority();
				if (lottery <= 0)
					return getThreadState(thread);
			}
			return null;
		}
	}
	
	protected class LotteryThreadState extends ThreadState {

		public LotteryThreadState(KThread thread) {
			super(thread);
			// TODO Auto-generated constructor stub
		}
		
		public void setPriority(int priority) {
			if (this.priority == priority)
				return;
			
			Lib.assertTrue(priorityMinimum <= priority && priority <= priorityMaximum);

			this.priority = priority;
			dirty();
		}
		
		public int getEffectivePriority() {
			if (!dirty)
				return effectivePriority;
			dirty = false;
			int effective = priority;
			for (PriorityQueue queue : resources)
				if (queue.transferPriority)
					effective += queue.getTicketSum();
			effectivePriority = effective;
			return effective;
		}
	}

	public static final int priorityMinimum = 1;
	public static final int priorityMaximum = Integer.MAX_VALUE;
}
