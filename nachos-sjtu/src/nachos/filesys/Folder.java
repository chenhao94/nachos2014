package nachos.filesys;

import java.util.Iterator;
import java.util.LinkedList;

import nachos.machine.Lib;

/**
 * Folder is a special type of file used to implement hierarchical filesystem.
 * It maintains a map from filename to the address of the file.
 * There's a special folder called root folder with pre-defined address.
 * It's the origin from where you traverse the entire filesystem.
 * 
 * @author starforever
 */
public class Folder extends GlobalFile
{
  /** the static address for root folder */
  public static int ROOT_ADDR = 1;
  
  /** the unique info of this folder;  
   *  the folder cannot be linked */
  private FolderEntry folderInfo;
  
  /** mapping from filename to folder entry */
  private LinkedList<FolderEntry> entries;
  
  @Deprecated
  private Folder (INode inode, FolderEntry info)
  {
	  super(inode);
	  folderInfo = info;
	  load();
  }
  
  static protected void createRoot(INode inode)
  {
	  Lib.assertTrue(FilesysKernel.realFileSystem.root_folder == null, "cannot create root directory twice.");
	  Lib.assertTrue(inode.inodeid == 2, "inode ID of root has to be 2.");
	  
	  FolderEntry entry = new FolderEntry("", inode);
	  FilesysKernel.realFileSystem.root_folder = new Folder(inode, entry);
	  FilesysKernel.realFileSystem.globalFiles.put(2, FilesysKernel.realFileSystem.root_folder);
	  FilesysKernel.realFileSystem.root_folder.entries.add(entry);
  }
  
  static public Folder getExistingFolder (INode inode)
  {
	  Lib.assertTrue(inode.file_type == INode.TYPE_FOLDER);
	  Folder folder = (Folder)FilesysKernel.realFileSystem.
				globalFiles.get(inode.inodeid);
	  if (folder == null)
	  {
		  /*folder = new Folder(inode);
		  FilesysKernel.realFileSystem.
		  		globalFiles.put(inode.inodeid, folder);*/
		  Lib.assertNotReached("folder not exist.");
	  }
	  return folder;
  }
  
  /** open a file in the folder and return its File object. 
   *  if not exist, return null.
   * */
  public File open (String filename, boolean file_type_only)
  {
	Iterator<FolderEntry> itr = entries.iterator();
	FolderEntry entry;
	GlobalFile file;
	itr.next(); // jumps over the parent dir
    while (itr.hasNext())
    {
    	entry = itr.next();
    	file = FilesysKernel.realFileSystem.globalFiles.get(entry.inodeID);
    	INode inode;
    	if (file == null)
    		itr.remove();
    	else if (entry.getName().equals(filename))
    	{
    		inode = file.inode;
    		if (inode.file_type == INode.TYPE_SYMLINK)
        	{
        		if (file.length() == 0)
        			return null;
        		byte[] buffer = new byte[2];
        		file.read(0, buffer, 0, 2);
        		inode = INode.getNthINode(Lib.bytesToInt(buffer, 0, 2));
        		if (inode == null)
        			return null;
        		file = FilesysKernel.realFileSystem.globalFiles.get(inode.inodeid);
        		if (file == null)
        			return null;
        	}
        	
    		if (file.inode.file_type != INode.TYPE_FILE && file_type_only)
    			return null;
    		file.inode.use();
    		return new File(file, filename);
    	}
    }
    return null;
  }
  
  /** create a new file in the folder and return its address.  
   *  if exists, set length to 0
   * */
  public File create (String filename)
  {
    File file = open(filename, true);
    if (file != null)
    	file.setLength(0);
    else
    {
    	if (this.getFile(filename) != null)
    		return null;
    	INode inode = new INode();
    	inode.setFileType(INode.TYPE_FILE);
    	file = new File(GlobalFile.getNewGlobalFile(inode), filename);
    	this.entries.add(new FolderEntry(filename, inode));
    	
    	/*GlobalFile gf = GlobalFile.getNewGlobalFile(inode);
    	int freesize = FilesysKernel.realFileSystem.getFreeSize();
    	Lib.debug('f', "create: size " + Lib.divRoundUp(gf.length(), Disk.SectorSize));
    	Lib.debug('f', "free size: " + freesize);*/
    }
    return file;
  }
  
  /** add an entry with specific filename and address to the folder */
  /*public void addEntry (String filename, int addr)
  /{
    //TODO implement this
  }*/
  
  /** remove an entry from the folder */
  /*public void removeEntry (String filename)
  {
  }*/
  
  /** save the content of the folder to the disk */
  public void save ()
  {
	Iterator<FolderEntry> itr = entries.iterator();
	FolderEntry entry;
	GlobalFile file;
	super.save();
	itr.next(); // jumps over the parent dir
	while (itr.hasNext())
    {
    	entry = itr.next();
    	file = FilesysKernel.realFileSystem.globalFiles.get(entry.inodeID);
    	if (file == null)
    		itr.remove();
    	else
    		file.save();
    }
  }
  
  /** load the content of the folder from the disk */
  public void load ()
  {
	  byte[] code = new byte[16];
	  entries = new LinkedList<FolderEntry>();
	  for (int i = 0; i < super.length(); i+=16)
	  {
		  super.read(i, code, 0, 16);
		  entries.add(FolderEntry.decode(code));
	  }
  }
  
  /*public FolderEntry getEntry(int index)
  {
	  return this.entries.get(index);
  }*/
  
  /**
   * entries list of this folder, parent included
   */
  public LinkedList<FolderEntry> getEntries()
  {
	  Iterator<FolderEntry> itr = entries.iterator();
	  FolderEntry entry;
	  while (itr.hasNext())
	  {
		  entry = itr.next();
		  if (FilesysKernel.realFileSystem.globalFiles.
				  get(entry.inodeID) == null)
			  itr.remove();
	  }
	  return this.entries;
  }
  
  
  public Folder getParent()
  {
	  return getExistingFolder(INode.getNthINode(entries.get(0).inodeID));
  }

  public Folder getSubDirectory(String foldername) 
  {
	  Iterator<FolderEntry> itr = entries.iterator();
	  FolderEntry entry;
	  GlobalFile folder;
	  itr.next(); // jumps over the parent dir
	  while (itr.hasNext())
	  {
		  entry = itr.next();
		  folder = FilesysKernel.realFileSystem.globalFiles.get(entry.inodeID);
		  if (folder == null)
			  itr.remove();
		  else if (entry.getName().equals(foldername))
		  {
			  if (folder.inode.file_type != INode.TYPE_FOLDER)
			  {
				  //System.err.println(foldername + " is not a directory.");
				  return null;
			  }
			  return getExistingFolder(folder.inode);
		  }
	  }
	  //System.err.println(foldername + ": no such file or directory.");
	  return null;
  }

  public String getName() 
  {
	  return this.folderInfo.getName();
  }

  /**
   * remove the specified file from current directory, 
   * acceptFolder bit define the behavior if that file is a folder, delete iff true
   * @param filename
   * @param acceptFolder
   */
  public int remove(String filename, boolean acceptFolder) 
  {
	  Iterator<FolderEntry> itr = entries.iterator();
	  FolderEntry entry;
	  GlobalFile file;
	  itr.next(); // jumps over the parent dir
	  while (itr.hasNext())
	  {
		  entry = itr.next();
		  file = FilesysKernel.realFileSystem.globalFiles.get(entry.inodeID);
		  if (file == null)
			  itr.remove();
		  else if (entry.getName().equals(filename))
		  {
			  INode inode = INode.getNthINode(entry.inodeID);
			  switch (inode.file_type)
			  {
			  case INode.TYPE_FILE:
				  break;
			  case INode.TYPE_SYMLINK:
				  break;
			  case INode.TYPE_FOLDER:
			  	  if (acceptFolder)
			  		  return Folder.removeFolder(inode, false);
			  default:
				  //System.err.println(filename + " is not a file, cannot be deleted.");
				  return -1;
			  }
			  inode.unlink();
			  itr.remove();
			  return 0;
		  }
	  }
	  return -1;
  }

  public int createFolder(String filename) 
  {
	  Iterator<FolderEntry> itr = entries.iterator();
	  FolderEntry entry;
	  Folder folder;
	  INode inode;
	  itr.next(); // jumps over the parent dir
	  while (itr.hasNext())
	  {
		  entry = itr.next();
		  if (FilesysKernel.realFileSystem.globalFiles.get(entry.inodeID) == null)
			  itr.remove();
		  else if (entry.getName().equals(filename))
		  {
			  inode = INode.getNthINode(entry.inodeID);
			  if (inode.file_type != INode.TYPE_FOLDER)
			  {
				  //System.err.println(filename + " exists, and not a directory. Create failed.");
				  return -1;
			  }
			  return 0;
		  }
	  }
	  inode = new INode();
	  inode.setFileType(INode.TYPE_FOLDER);
	  entry = new FolderEntry(filename, inode);
	  folder = new Folder(inode, entry);
	  FilesysKernel.realFileSystem.globalFiles.put(inode.inodeid, folder);
	  folder.entries.add(this.folderInfo);
	  this.entries.add(entry);
	  return 0;
  }

  /** remove a specific folder, empty bit is true when we want remove empty folder only */
  public int removeFolder(String filename, boolean empty)
  {
	  Iterator<FolderEntry> itr = entries.iterator();
	  FolderEntry entry;
	  INode inode;
	  itr.next(); // jumps over the parent dir
	  while (itr.hasNext())
	  {
		  entry = itr.next();
		  if (FilesysKernel.realFileSystem.globalFiles.get(entry.inodeID) == null)
			  itr.remove();
		  else if (entry.getName().equals(filename))
		  {
			  inode = INode.getNthINode(entry.inodeID);
			  if (inode.file_type != INode.TYPE_FOLDER)
			  {  
				  //System.err.println(filename + " exists, and not a directory. Remove failed.");
			  }
			  else
			  {  
				  itr.remove();
				  return Folder.removeFolder(inode, empty);
			  }
			  return -1;
		  }
	  }
	  //System.err.println(filename + ": not exist");
	  return -1;
  }

  /** remove a specific folder, empty bit is true when we want remove empty folder only */
  private static int removeFolder(INode inode, boolean empty)
  {
	  Folder folder = Folder.getExistingFolder(inode);
	  for (FolderEntry entry : folder.entries)
	  {
		  if (entry.equals(folder.entries.get(0)))
			  continue;
		  if (FilesysKernel.realFileSystem.globalFiles.get(entry.inodeID) == null)
			  continue;
		  if (empty)
			  return -1;
		  INode entryInode = INode.getNthINode(entry.inodeID);
		  switch (entryInode.file_type)
		  {
		  case INode.TYPE_FILE:
			  break;
		  case INode.TYPE_SYMLINK:
			  GlobalFile linkfile = SymLink.getSymlink(entryInode).dst; 
			  if (linkfile != null)
				  linkfile.inode.unlink();
			  break;
		  case INode.TYPE_FOLDER:
		  	  Folder.removeFolder(entryInode, empty);
		  	  continue;
		  }
		  entryInode.unlink();
	  }
	  inode.unlink();
	  return 0;
  }

  public GlobalFile getFile(String filename)
  {
	  for (FolderEntry entry : entries)
	  {
		  INode entryInode = INode.getNthINode(entry.inodeID);
		  if (entry.getName().equals(filename) && !entry.equals(entries.get(0)))
			  return GlobalFile.getNewGlobalFile(entryInode);
	  }
	  return null;
  }

  public int createLink(String srcName, GlobalFile dstFile) 
  {
	  if (dstFile == null)
		  return -1;
	  for (FolderEntry entry : entries)
		  if (entry.getName().equals(srcName) && !entry.equals(entries.get(0)))
			  return -1;
	  INode inode = dstFile.inode;
	  inode.link();
	  entries.add(new FolderEntry(srcName, inode));
	  return 0;
  }
  
  public int createSymlink(String srcName, GlobalFile dstFile) 
  {
	  INode inode;
	  for (FolderEntry entry : entries)
		  if (entry.getName().equals(srcName) && !entry.equals(entries.get(0)))
			  return -1;
	  inode = new INode();
	  SymLink.createNewSymlink(inode, dstFile);
	  entries.add(new FolderEntry(srcName, inode));
	  return 0;
  }
  
}
