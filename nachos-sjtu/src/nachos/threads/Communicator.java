package nachos.threads;

import nachos.machine.Lib;

/**
 * A <i>communicator</i> allows threads to synchronously exchange 32-bit
 * messages. Multiple threads can be waiting to <i>speak</i>, and multiple
 * threads can be waiting to <i>listen</i>. But there should never be a time
 * when both a speaker and a listener are waiting, because the two threads can
 * be paired off at this point.
 */
public class Communicator {
	/**
	 * Allocate a new communicator.
	 */
	public Communicator() {
	}

	/**
	 * Wait for a thread to listen through this communicator, and then transfer
	 * <i>word</i> to the listener.
	 * 
	 * <p>
	 * Does not return until this thread is paired up with a listening thread.
	 * Exactly one listener should receive <i>word</i>.
	 * 
	 * @param word
	 *            the integer to transfer.
	 */
	public void speak(int word) {
		msgPassLock.acquire();
		while (message != null)
			chanceToSpeak.sleep();
		Lib.assertTrue(message==null);
		message = word;
		someWaitForWrite.wake();
		while (message != null)
			someWaitForRead.sleep();
		Lib.assertTrue(message==null);
		chanceToSpeak.wake();
		msgPassLock.release();
	}

	/**
	 * Wait for a thread to speak through this communicator, and then return the
	 * <i>word</i> that thread passed to <tt>speak()</tt>.
	 * 
	 * @return the integer transferred.
	 */
	public int listen() {
		msgPassLock.acquire();
		while (message == null)
			someWaitForWrite.sleep();
		int msg = message.intValue();
		message = null;
		someWaitForRead.wake();
		msgPassLock.release();
		return msg;
	}
	
	private Lock msgPassLock = new Lock();
	private Condition someWaitForRead = new Condition(msgPassLock);
	private Condition someWaitForWrite = new Condition(msgPassLock);
	private Condition chanceToSpeak = new Condition(msgPassLock);
	//Pair<KThread, Integer> waitForWrite;
	Integer message = null;
}
