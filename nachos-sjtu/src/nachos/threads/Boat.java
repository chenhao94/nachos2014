package nachos.threads;

import nachos.ag.BoatGrader;
import nachos.machine.Lib;

public class Boat {
	static BoatGrader bg;

	public static void selfTest() {
		BoatGrader b = new BoatGrader();

		System.out.println("\n ***Testing Boats with only 2 children***");
		begin(0, 2, b);

		// System.out.println("\n ***Testing Boats with 2 children, 1 adult***");
		// begin(1, 2, b);

		// System.out.println("\n ***Testing Boats with 3 children, 3 adults***");
		// begin(3, 3, b);
	}

	public static void begin(int adults, int children, BoatGrader b) {
		// Store the externally generated autograder in a class
		// variable to be accessible by children.
		bg = b;
		adultOnOahu = adults;
		childOnOahu = children;

		// Instantiate global variables here

		// Create threads here. See section 3.4 of the Nachos for Java
		// Walkthrough linked from the projects page.

		Runnable adult_r = new Runnable() {
			public void run() {
				AdultItinerary();
			}
		};
		Runnable child_r = new Runnable() {
			public void run() {
				ChildItinerary();
			}
		};
		KThread t = null;
		//t.setName("Sample Boat Thread");
		//t.fork();
		for (int i = 0; i < adults; ++i)
		{
			t = new KThread(adult_r);
			t.setName("Adult " + (new Integer(i)).toString());
			t.fork();
		}
		
		for (int i = 0; i < children; ++i)
		{
			t = new KThread(child_r);
			t.setName("Child " + (new Integer(i)).toString());
			t.fork();
		}
		
		communicator.listen();
	}

	static void AdultItinerary() {
		/*
		 * This is where you should put your solutions. Make calls to the
		 * BoatGrader to show that it is synchronized. For example:
		 * bg.AdultRowToMolokai(); indicates that an adult has rowed the boat
		 * across to Molokai
		 */
		Side side = Side.Oahu;
		boatAccess.acquire();
		while (boatSide != side || childOnOahu > 1)
		{
			if (boatSide != side)
				boatOnOahu.sleep();
			if (childOnOahu > 1)
				noMoreThan2.sleep();
		}
		Lib.assertTrue(boatSide == side && childOnOahu < 2);
		bg.AdultRowToMolokai();
		side = boatSide = Side.Molokai;
		boatOnMolokai.wake();//----
		--adultOnOahu;
		moreChildrenOrNoAdults.wakeAll();
		boatAccess.release();
		KThread.finish(); // arrive
	}

	static void ChildItinerary() {
		Side side = Side.Oahu;
		while (true)
		{
			boatAccess.acquire();
			if (side == Side.Molokai)
			{
				if (childOnOahu == 0 && adultOnOahu == 0)
				{
					communicator.speak(0);
					KThread.finish();
				}
				while (boatSide != side)
					boatOnMolokai.sleep();
				Lib.assertTrue(boatSide == side);
				bg.ChildRowToOahu();
				++childOnOahu;
				moreChildrenOrNoAdults.wake(); // ---
				side = boatSide = Side.Oahu;
				boatOnOahu.wakeAll();
			}
			else
			{
				while (boatSide != side || (childOnOahu == 1 && adultOnOahu > 0))
				{
					if (boatSide != side)
						boatOnOahu.sleep();
					if (childOnOahu == 1 && adultOnOahu > 0)
						moreChildrenOrNoAdults.sleep();
				}
				Lib.assertTrue(childOnOahu > 1 || adultOnOahu == 0);
				if (rideAvailable)
				{
					bg.ChildRideToMolokai();
					rideAvailable = false;
					childOnOahu -= 2;
					noMoreThan2.wake(); // --- 
					side = boatSide = Side.Molokai;
					boatOnMolokai.wake(); // --- 
				}
				else
				{
					bg.ChildRowToMolokai();
					side = Side.Molokai;
					if (childOnOahu > 1)
					{
						rideAvailable = true;
						moreChildrenOrNoAdults.wake(); // --- 
					}
					else
					{	
						boatSide = side;
						--childOnOahu;
						boatOnMolokai.wake(); // --- 
						noMoreThan2.wake(); // --- 
					}
				}
			}
			boatAccess.release();
		}
	}

	static void SampleItinerary() {
		// Please note that this isn't a valid solution (you can't fit
		// all of them on the boat). Please also note that you may not
		// have a single thread calculate a solution and then just play
		// it back at the autograder -- you will be caught.
		System.out
				.println("\n ***Everyone piles on the boat and goes to Molokai***");
		bg.AdultRowToMolokai();
		bg.ChildRideToMolokai();
		bg.AdultRideToMolokai();
		bg.ChildRideToMolokai();
	}
	
	static public enum Side { Oahu, Molokai };
	static private Side boatSide = Side.Oahu;
	static private Lock boatAccess = new Lock();
	static private Condition boatOnOahu = new Condition(boatAccess);
	static private Condition boatOnMolokai = new Condition(boatAccess);
	static private Condition noMoreThan2 = new Condition(boatAccess);
	static private Condition moreChildrenOrNoAdults = new Condition(boatAccess);
	static private boolean rideAvailable = false;
	static private int childOnOahu = 0, adultOnOahu = 0;
	static private Communicator communicator = new Communicator();
}
