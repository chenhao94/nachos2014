	.file	1 "test.c"
	.globl	global_value
	.data
	.align	2
global_value:
	.word	566
	.rdata
	.align	2
$LC0:
	.ascii	"msg\n\000"
	.align	2
$LC1:
	.ascii	"Hello, world! BTW, global_value = %d, local_value = %d\n"
	.ascii	"\000"
	.align	2
$LC2:
	.ascii	"I'm going to print my self, and copy myself to test2.c\n"
	.ascii	"\000"
	.align	2
$LC3:
	.ascii	"test.c\000"
	.align	2
$LC4:
	.ascii	"test2.c\000"
	.text
	.align	2
	.globl	main
	.ent	main
main:
	.frame	$fp,40,$31		# vars= 16, regs= 2/0, args= 16, extra= 0
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	subu	$sp,$sp,40
	sw	$31,36($sp)
	sw	$fp,32($sp)
	move	$fp,$sp
	jal	__main
	li	$2,777			# 0x309
	sw	$2,16($fp)
	li	$4,1			# 0x1
	la	$5,$LC0
	li	$6,4			# 0x4
	jal	write
	la	$4,$LC1
	lw	$5,global_value
	lw	$6,16($fp)
	jal	printf
	la	$4,$LC2
	jal	printf
	la	$4,$LC3
	jal	open
	sw	$2,20($fp)
	la	$4,$LC4
	jal	creat
	sw	$2,24($fp)
$L2:
	lw	$4,20($fp)
	la	$5,buffer
	li	$6,128			# 0x80
	jal	read
	sw	$2,28($fp)
	lw	$2,28($fp)
	bgtz	$2,$L4
	j	$L3
$L4:
	li	$4,1			# 0x1
	la	$5,buffer
	lw	$6,28($fp)
	jal	write
	lw	$4,24($fp)
	la	$5,buffer
	lw	$6,28($fp)
	jal	write
	j	$L2
$L3:
	lw	$4,20($fp)
	jal	close
	lw	$4,24($fp)
	jal	close
	move	$2,$0
	move	$sp,$fp
	lw	$31,36($sp)
	lw	$fp,32($sp)
	addu	$sp,$sp,40
	j	$31
	.end	main

	.comm	buffer,128
